
<?php
// Start the session
session_start();
?>
<?php
/*
Template Name: renew membership2
*/
?>

<?php
//error handler function
function customError($errno, $errstr) {
  echo "<b>Error:</b> [$errno] $errstr";
}

//set error handler
set_error_handler("customError");
?>

<?php
if ( !defined('ABSPATH')) exit; // Exit if accessed directly
/**
 * Standard page output (Default template)
 */
?>
<?php	$sb_layout = weaverx_page_lead( 'page' ); ?>
<?php	weaverx_sb_precontent('page');?>
 <?php
    $membership_id =  $email =  $postcodeErr = $emailErr= "";
    
	$submit = "correct" ;		
     if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if(empty($_POST["membership_id"])){
		   	$submit = "membership_id";
            $postcodeErr = "* membership number is empty";
		  
       }else{
		    $membership_id = test_input($_POST["membership_id"]);
		    $numLength = strlen( $membership_id );
           // check if membership_id is a number and length is equal to 4 
		   if (($numLength != 6) || (!is_numeric($membership_id))) {
				$postcodeErr = "* membership number is not valid";
				$membership_id = "";
				$submit = "membership_id";
		   }else{
		      	$_SESSION['membership_id'] = $membership_id;  
		   } 
	    }
    if(empty($_POST["email"])){
		   	$submit = "email";
            $emailErr = "* Email is required";
		  
       }else{
		    $email = test_input($_POST["email"]);
		    $_SESSION['email'] = $email;
             // check if e-mail address is well-formed
                if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $emailErr = "* Invalid email format";
                    $submit = "email";
                  }else{
                     $_SESSION['email'] = $email;
                  }
	    }
                
    }
    
    // validate all input data for illegell characters
    function test_input($data) {
      $data = trim($data);
      $data = stripslashes($data);
      $data = htmlspecialchars($data);
      return $data;
    }
?>


<?php if($submit != "correct"){ ?>
<form name="myForm3" action="http://www.ozbizonline.com.au/?page_id=492" method="post" >
    <fieldset>
        <legend>Please go back and correct following errors</legend>
        <span class="error"><?php echo $postcodeErr ; ?></span>
        <br>
        <span class="error"> <?php echo $emailErr ; ?> </span
    </fieldset>
    <input type="submit" value="go back">
</form>
    
<?php }else{ 
  echo "<meta http-equiv='refresh' content='0;url=http://www.ozbizonline.com.au/?page_id=497'>";
exit();
 } ?>




<?php	while ( have_posts() ) {
		weaverx_post_count_clear(); the_post();

		get_template_part( 'templates/content', 'page' );

		comments_template( '', true );
	}

	weaverx_sb_postcontent('page');

	weaverx_page_tail( 'page', $sb_layout );    // end of page wrap
?>