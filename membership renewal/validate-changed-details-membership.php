
<?php
// Start the session
session_start();
?>
<?php
/*
Template Name: validate edited membership details
*/
?>

<?php
if ( !defined('ABSPATH')) exit; // Exit if accessed directly
/**
 * This page does back-end validation and saves the edited information of an already existing member
 */
?>
<?php	$sb_layout = weaverx_page_lead( 'page' ); ?>
<?php	weaverx_sb_precontent('page');?>
 
<?php
// define variables and set to empty values
$surnameErr = $pnameErr = $forenameErr = $titleErr= "";
$yobErr = $familyInfoErr = $addressErr  = $user_id = $emailErr = $branchErr ="";
$stateErr = $confirmEmailErr = $genderErr = $suburbErr = $conditionErr = $postcodeErr = $contactErr = "";
$surname = $forename = $title = $emergency_relationship=   $findUs = $membership_id = "";
$emergency_name= $membership_class = $membership_duration =$emergency_number =$enews ="";
$yob = $email = $gender = $branch = $expiry_date= $membership2 = $address1="";
$address2= $join_date =$postal_address = $state = $work_tel = $mob = $tel = $condition = $telCode = $pname = $suburb = $postcode = $confirmEmail = "";
$surnameFamily = $forenameFamily = $titleFamily  =  $enews = $emergencyErr = $yobFamily = $pnameFamily = $mobFamily = $genderFamily= "";
$submit = "correct" ;

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (empty($_POST["membership_id"])) {
    $submit= "membership_id";
    } else {
        $membership_id = test_input($_POST["membership_id"]);
   }
       
    if(empty($_POST["country"])){
		   	$submit = "country";
		  
       }else{
		    $country = test_input($_POST["country"]);
			 $_SESSION['country'] = $country ;
			
	}
	   
   if(empty($_POST["membership2"])){
		   	$submit = "membership2";
		  
       }else{
		     $membership2 = test_input($_POST["membership2"]);
		     $_SESSION['membership2'] = $membership2;
					  if( $membership2 == "family"){
							  if(empty($_POST["surnameFamily"]) || empty($_POST["titleFamily"]) || empty($_POST["genderFamily"]) || empty($_POST["yobFamily"]) ){
							   $familyInfoErr = "* Family member's information is missing";
							   $submit = $submit = "familiymembership";;
							  }else{
								$surnameFamily = test_input($_POST["surnameFamily"]);
								$_SESSION['surnameFamily'] = $surnameFamily;
								$fornameFamily = test_input($_POST["foreNameFamily"]);
								$_SESSION['fornameFamily'] = $fornameFamily;
								$pnameFamily = test_input($_POST["preferNameFamily"]);
								$_SESSION['pnameFamily'] = $pnameFamily;
								$titleFamily = test_input($_POST["titleFamily"]);
								 $_SESSION['titleFamily'] = $titleFamily ;
								$yobFamily = test_input($_POST["yobFamily"]);
								 $_SESSION['yobFamily'] = $yobFamily ;
								$mobFamily = test_input($_POST["mobFamily"]);
								$_SESSION['mobFamily'] = $mobFamily ;
								 $genderFamily = test_input($_POST["genderFamily"]);
								 $_SESSION['genderFamily'] = $genderFamily  ;
							  }
					  }
					
		   }
           
           if(empty($_POST["membership"])){
		   	$submit = "membership";
		  
       }else{
		    $branch = test_input($_POST["membership"]);
			$branchName2 = test_input($_POST["branchName2"]);
		    $_SESSION['direct_branch'] = $branch;
            $membership =$branch;
			 $_SESSION['branchName2'] = $branchName2;
            
		     if( ($branch=="branch") && empty($_POST["branchName2"])) {
					$branchErr = "* You must select a branch name for your branch membership";
					$submit = "membership";
		     }
			
	   }
   
  if (empty($_POST["surname"])) {
    $surnameErr = "* Name is required";
	$submit = "surname" ;
	
  } else {
    $surname = test_input($_POST["surname"]);
	$_SESSION['surname'] = $surname;
    // check if name only contains letters and whitespace
    if (!preg_match("/^[a-zA-Z ]*$/",$surname)) {
      $surnameErr = "* Only letters and white space allowed in name";
	  $submit = "surname" ;
    }
  }
  
   if (empty($_POST["state"])) {
    $state= "";
	$submit = "state" ;
  } else {
    $state = test_input($_POST["state"]);
	$stateOther = test_input($_POST["stateOther"]);
	$_SESSION['state'] = $state;
	$_SESSION['stateOther'] = $stateOther;
	if ($state == "other" && empty($_POST["stateOther"])){
		   $stateErr = "You have not selected the name of your state";
		   $submit = "stateOther" ;
	}
	
  }
  
  if (empty($_POST["forename"])) {
    $forenameErr = " * forename is required";
	$submit = "forename" ;
  } else {
    $forename = test_input($_POST["forename"]);
	$_SESSION['forename'] = $forename;
    // check if name only contains letters and whitespace
    if (!preg_match("/^[a-zA-Z ]*$/",$forename)) {
      $forenameErr = "*Only letters and white space allowed";
	  $submit = "foreneme" ;
    }
  }

  if (empty($_POST["pname"])) {

    }
   else {
    $pname = test_input($_POST["pname"]);
    $_SESSION['pname'] = $pname;
	 if (!preg_match("/^[a-zA-Z ]*$/",$pname)) {
      $pnameErr = " * Only letters and white space allowed";
	  $submit = "pname" ;
		   }
    }

   if (empty($_POST["gender"])) {
    $genderErr = "*Gender is required";
	$submit = "gender" ;
  } else {
    $gender = test_input($_POST["gender"]);
    $_SESSION['gender'] = $gender;
  }
  
  if (empty($_POST["emergency_name"])) {
    $emergencyErr = "*Emergency name is required";
	$submit = "emergencyName" ;
  } else {
    $emergency_name = test_input($_POST["emergency_name"]);
    $_SESSION['emergency_name'] = $emergency_name;
  }
   if (empty($_POST["emergency_number"])) {
    $emergencyErr = "*Emergency number is required";
	$submit = "emergencyNumber" ;
  } else {
    $emergency_number = test_input($_POST["emergency_number"]);
    $_SESSION['emergency_number'] = $emergency_number;
  }
   if (empty($_POST["emergency_relationship"])) {
    $emergencyErr = "*Emergency contact relationship is required";
	$submit = "emergencyRelation" ;
  } else {
    $emergency_relationship = test_input($_POST["emergency_relationship"]);
    $_SESSION['emergency_relationship'] = $emergency_relationship;
  }

   if (empty($_POST["title"])) {
    $titleErr = "* Title is required";
	$submit = "title" ;
  } else {
    $title = test_input($_POST["title"]);
    $_SESSION['title'] = $title;
	if($title == "oth"){
		   $_SESSION['title_other'] = test_input($_POST["title_other"]);;
	}
  }

   if (empty($_POST["birthyear"])) {
    $yobErr = "* Birth year is required";
	$submit = "yob" ;
  } else {
    $yob = test_input($_POST["birthyear"]);
    $_SESSION['yob'] = $yob;
  }
  
    if (empty($_POST["address1"]) && empty($_POST["address2"])) {
    $addressErr = "*address is required";
	$submit = "address";
  } else {
    $address1 = test_input($_POST["address1"]);
    $_SESSION['address1'] = $address1;
	$address2 = test_input($_POST["address2"]);
	$_SESSION['address2'] = $address2;
  }
   
  if (empty($_POST["suburb"])) {
    $suburbErr = "* suburb is required";
	$submit = "suburb";
  } else {
    $suburb = test_input($_POST["suburb"]);
	$_SESSION['suburb'] = $suburb;
  }

  if (empty($_POST["post_code"])) {
    $postcodeErr = "* postcode is required";
	$submit = "postcode";
  } else {
    $postcode = test_input($_POST["post_code"]);
	$_SESSION['postcode'] = $postcode;
	$numLength = strlen($postcode);
    // check if post code is a number and length is equal to 4 
		   if (($numLength != 4) || (!is_numeric($postcode))) {
				$postcodeErr = "* postcode is not valid";
				$postcode = "";
				$submit = "postcode";
		   }else{
		       // validate the post codes for indvidual states
		       validate_state_post_code($state,$postcode,$postcodeErr);	  
		   } 
  }

  if (empty($_POST["email"])) {
    $emailErr = "* Email is required";
	$submit = "email";
  } else {
    $email = test_input($_POST["email"]);
	$_SESSION['email'] = $email;
    // check if e-mail address is well-formed
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $emailErr = "* Invalid email format";
	  $submit = "email";
    }
  }

   if ((empty($_POST["tel"])) && (empty($_POST["mob"])) && (empty($_POST["work_tel"])) ) {
   //if (empty($_POST["mob"])) {
    $contactErr = "* You must Enter a contact number";
	$submit = "contactNumber";
  } else {
    $tel = test_input($_POST["tel"]);
	$_SESSION['tel'] = $tel;
	$work_tel = test_input($_POST["work_tel"]);
	$_SESSION['work_tel'] = $work_tel;
	$mob = test_input($_POST["mob"]);
	$_SESSION['mob'] = $mob;

   }

  if (empty($_POST["find_us"])) {
    $find_us = "";
  } else {
		   $findUs = test_input($_POST["find_us"]);
		   $_SESSION['findUs'] = $findUs;
		   if($findUs == "oth_findus"){
				 $_SESSION['findUs'] =  test_input($_POST["findus_other"]);	  
		   }
		   
    }
    if (empty($_POST["membership_duration"])) {
    
  } else {
    $membership_duration = test_input($_POST["membership_duration"]);
	$_SESSION['membership_duration'] = $membership_duration;
	echo("membership duration").$membership_duration;
    
  }

  if (empty($_POST["interest"])) {
    $interest= "";
  } else {
    $interest = test_input($_POST["interest"]);
	$_SESSION['interest'] = $interest;
  }
    if (empty($_POST["postal_address"])) {
   
    $postal_address= "";
  } else {
   $postal_address = test_input($_POST["postal_address"]);
	$_SESSION['postal_address'] =   $postal_address;
  }
  
   if (empty($_POST["membership_class"])) {
    $membership_class = "";
  } else {
   $membership_class = test_input($_POST["membership_class"]);
	$_SESSION['membership_class'] =   $membership_class;
  }
    
	if (empty($_POST["expiry_date"])) {
            $error= 'expiry_date';
    } else {
            $expiry_date = test_input($_POST["expiry_date"]);
        }
	$join_date = test_input($_POST["join_date"]);
}

// validate all input data for illegell characters
function test_input($data) {
   $data = trim($data);
   $data = substr($data, 0, 500);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  $data = EscapeShellCmd($data);
  return $data;
}

//function to validate post codes for a state
function validate_state_post_code($state, $postcode,$postcodeErr ){
   if(($state == "nsw" && $postcode[0] != "2") ||
	  ($state == "nt" &&  $postcode[0] != "0")  ||
        ($state == "qld" && $postcode[0] != "4") ||
        ($state == "sa" && $postcode[0] != "5")  ||
        ($state == "tas" && $postcode[0] != "7") ||
        ($state == "vic" && $postcode[0] != "3") ||
        ($state == "wa" && $postcode[0] != "6") 
	  ){
		    $GLOBALS['postcodeErr'] = "* Post code for the seleted state is not right";
			$postcodeErr = "* Post code for the seleted state is not right";
	         $submit = "postCodeState";
   }else{

   }
}
//action="<?php echo htmlspecialchars($_SERVER["REQUEST_URI"]);"
?>

<?php
$html = <<<HTML
<form name="myForm" method="post" >
<fieldset>
<legend>Please go back and correct following errors</legend>
<span class="error"> $surnameErr</span>
<span class="error"> $pnameErr </span>
<br>
<span class="error"> $titleErr</span>
<br>
<span class="error"> $genderErr </span>
<br>
<span class="error"> $yobErr </span>
<br>
<span class="error">$addressErr</span>
<br>
<span class="error">$suburbErr</span>
<br>
<span class="error"> $stateErr</span>
<br>
<span class="error"> $postcodeErr</span>
<br>
<br>
<span class="error"> $emergencyErr</span>
<br>
<span class="error"> $branchErr</span>
<br>
<span class="error"> $emailErr </span>
<br>
<span class="error">$confirmEmailErr</span>
<br>
<span class="error">$conditionErr</span>
<br>
<span class="error">$familyInfoErr</span>
</fieldset>
</form>
HTML;
?>


<?php if($submit != "correct"){ 
echo $submit; 
echo $html ;
$goBack = <<<GOBACK
<form action="http://www.ozbizonline.com.au/edit-membership-details/" method="post">   
    <input type="Submit" value="Go Back">
</form>
GOBACK;
echo $goBack;
} else{ 
//echo "<meta http-equiv='refresh' content='0;url=http://www.ozbizonline.com.au/save-membership-details/'>";
//exit();
 // save user to gsq_users table in database
	 $servername = "localhost" ;
    $username = "gsqtest_gsqAdmin";
    $password = "gsqProject2016";
    $dbname = "gsqtest_users";

// Create connection
			$conn = new mysqli($servername, $username, $password, $dbname);
				// Check connection
			if ($conn->connect_error) {
				die("Connection failed: " . $conn->connect_error);
			} 
			$surname = mysqli_real_escape_string($conn, $surname);
			$forename = mysqli_real_escape_string($conn, $forename);
			$email = mysqli_real_escape_string($conn, $email);
			$yob = mysqli_real_escape_string($conn,$yob);
			$address1 = mysqli_real_escape_string($conn, $address1);
			$address2 = mysqli_real_escape_string($conn, $address2);
			$suburb = mysqli_real_escape_string($conn, $suburb);
			$state= mysqli_real_escape_string($conn, $state);
			$country = mysqli_real_escape_string($conn, $country);
			$postcode = mysqli_real_escape_string($conn, $postcode);
			$tel = mysqli_real_escape_string($conn,$tel);
			$work_tel = mysqli_real_escape_string($conn, $work_tel);
			$mob = mysqli_real_escape_string($conn, $mob);
			$title = mysqli_real_escape_string($conn, $title);
			$findUs= mysqli_real_escape_string($conn, $findUs);
			$interest = mysqli_real_escape_string($conn, $interest);
			$postal_address = mysqli_real_escape_string($conn, $postal_address);
			$membership_duration = mysqli_real_escape_string($conn,$membership_duration);
			$membership_class = mysqli_real_escape_string($conn,$membership_class);
			$emergency_name = mysqli_real_escape_string($conn,$emergency_name);
			$emergency_number = mysqli_real_escape_string($conn,$emergency_number);
			$emergency_relationship = mysqli_real_escape_string($conn,$emergency_relationship);
			$membership_id = mysqli_real_escape_string($conn,$membership_id);
			$membership = mysqli_real_escape_string($conn,$membership);
			if($country != "Australia"){
			  $state = "" ;
			}
	
			    $todays_date =time();
				$expiry_date =  strtotime($expiry_date);
				
				$difference  = $todays_date-$expiry_date;
				$difference = floor($difference / (60 * 60 * 24)); // calculate number of days
				// check for grace period
				if($difference < 60){
					  $renewal_untill2 = date("Y-m-d H:i:s",strtotime("+$membership_duration years",  $expiry_date));
				
				}else{
				 
						$years_of_renewal = $membership_duration;
						$renewal_untill=strtotime("$years_of_renewal Years");
						$renewal_untill2 = date("Y-m-d H:i:s", $renewal_untill);
				}
			
				  // find since how many years a member
				  $diff2 =  strtotime(date("Y-m-d H:i:s")) - strtotime($join_date);
			       $diff2= floor($diff2 / (60 * 60 * 24*365)); // calculate number of days
				  $years_a_member = $diff2;
                 
				  if($membership == "direct"){
					$is_direct = "true" ;
				  }else{
					$is_direct = "false" ;
				  }
				  if($membership2 == "family"){
					$is_single = "false" ;
				  }else if($membership2 == "single"){
					$is_single = "true" ;
				  }else if($membership2 == "life"){
					// life enduring must be for single and for whole life of the member
					 $is_single = "true" ;
					 $years_of_renewal = 200 ;
					 $renewal_untill=strtotime("200 Years");
				     $renewal_untill2 = date("Y-m-d H:i:s", $renewal_untill);
				  }else{}
			$sql="UPDATE `gsq_members` SET 
			`years_of_renewal`=?,`renewal_untill`=?,`years_a_member`=?,
			`membership_class`=?,`is_direct`=?,`branch`=?,`is_single`=?,
			`interest_areas`=?,`find_about`=?
			WHERE membership_id = ?";
			if ($stmt = $conn->prepare($sql))
            {
							   $stmt->bind_param("dsssssssss",
												 $years_of_renewal,$renewal_untill2,
												 $years_a_member,$membership_class,
												 $is_direct,$branch,$is_single,$interest,$findUs,$membership_id	);
							   $stmt->execute(); 
                               if ($stmt->errno) {
                                    die ("FAILURE!!! " . $stmt->error);
                                }
                                   $stmt->close();
                                   
                                   // get the user_id of last updated record to find the corresponding user in gsq_user table
                                   $sql2 = "SELECT  `user_ID`
                                            FROM  `gsq_members` 
                                            WHERE membership_ID =  ? ";
                                    if($stmt2 = $conn->prepare($sql2))
                                    {
                                                    $stmt2->bind_param("s",$membership_id);
                                                    $stmt2->execute();
                                                    $stmt2->store_result();
                                                   if ($stmt2->num_rows == 0){
                                                           die("Error: No result found");
                                                   }else
                                                   {
                                                       $stmt2->bind_result( $user_id );
                                                       $stmt2->fetch();
                                                       $stmt2->close();
                                                      // Now update the details in gsq_user table
                                                       $sql3 = "UPDATE `gsq_users` SET `surname`= ?,`first_name`=?,`gender`=?,
                                                               `email`=?,`birth_year`=?,`title`=?,`address1`=?,
                                                               `address2`=?,`city`=?,`state`=?,`country`=?,
                                                               `postcode`=?,`postal_address`=?,`home_phone`=?,`work_phone`=?,
                                                               `mobile`=?,`emergency_name`=?,`emergency_number`=?,`emergency_relationship`=?
                                                               WHERE `user_ID` = ? ";
                   
                                                                // Attempt to prepare the query
                                                               if ($stmt3 = $conn->prepare($sql3)) {
                                                        
                                                                            $stmt3->bind_param("sssssssssssssssssssd",
                                                                                               $surname , $forename , $gender , $email,
                                                                                               $yob,$title, $address1, $address2 , $suburb,
                                                                                               $state,$country,
                                                                                               $postcode,$postal_address, $tel ,$work_tel ,$mob ,
                                                                                               $emergency_name,$emergency_number,$emergency_relationship,$user_id);
                                                                                               $stmt3->execute(); 
                                                                                               if ($stmt3->errno)
                                                                                               {
                                                                                                     die ("FAILURE!!! " . $stmt3->error);
                                                                                               }
                                                                                                $stmt3->close();
                                                                                                $conn->close(); ?>
                                                                                                <!-- send an email to the member -->
                                                                                                <div >
                                                                                                        <h4>Your membership details are successfully changed. </h4>
                                                                                                        <p>Use following information to use MYGSQ: <br>
                                                                                                        Username: <strong> <?php echo htmlspecialchars($email) ;?><br> </strong>
                                                                                                        Password: <strong> <?php echo htmlspecialchars($membership_id) ;?> </strong>
                                                                                                         </p>
                                                                                                       <p>Press submit if you want to send an email with your login details to: <?php echo "$email"; ?></p>
                                                                                                       <form   action="http://www.ozbizonline.com.au/email-handler/ " method="post">
                                                                                                            <input type="hidden" name="first_name" value="<?php echo "$membership_id"; ?>"><br>
                                                                                                           <input type="hidden" name="email" value="<?php echo "$email"; ?>"><br>
                                                                                                           <input type="submit"  class="sendIt" name="submit" value="Submit">
                                                                                                       </form>
                                                                                                </div>
                                                       <?php            }else{
                                                                            session_unset();
                                                                            session_destroy();
                                                                            echo "Error: " . "<br>" . $conn->error;
                                                                            // die ("FAILURE!!! " . $stmt3->error);
                                                                   }
                                                   }
                                    }else{
                                        session_unset();
                                        session_destroy();
                                        echo "Error: " . $sql . "<br>" . $conn->error;
                                    }
                                ?>
							   
	   <?php }else{
				session_unset();
				session_destroy();
				echo "Error: " . $sql . "<br>" . $conn->error;
			}
						
  }
?>

<?php	while ( have_posts() ) {
		weaverx_post_count_clear(); the_post();

		get_template_part( 'templates/content', 'page' );

		comments_template( '', true );
	}

	weaverx_sb_postcontent('page');

	weaverx_page_tail( 'page', $sb_layout );    // end of page wrap
?>