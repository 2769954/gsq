
<?php
// Start the session
session_start();
?>
<?php
/*
Template Name: membership renewal after validation
*/
?>

<?php
if ( !defined('ABSPATH')) exit; // Exit if accessed directly
/**
 * Standard page output (Default template)
 */
?>
<?php	$sb_layout = weaverx_page_lead( 'page' ); ?>
<?php	weaverx_sb_precontent('page');?>
 <?php
    $surname = $is_direct =$is_single = $firstname = $title = $birth_year= $membership_id= $branch = $membership_class= $home_phone= $work_phone=$mobile=$interest_areas=$enews = $yob = $email = $gender = $branch = $membership2 = $address1= $address2 = $state = $work_tel = $mob = $tel = $condition = $telCode = $includeMe = $pname = $suburb = $postcode = $confirmEmail = "";
    $surnameFamily = $forenameFamily = $titleFamily = $yobFamily = $pnameFamily = $mobFamily = $genderFamily= "";
	$records_found = true ;		// if records of membership with given membeship_id and emailid do exist
			 if (isset($_SESSION['membership_id'])){
               $membership_id = $_SESSION['membership_id'];
			
            }
			
			 if (isset($_SESSION['email'])){
                $email =  $_SESSION["email"];                 
            }
	// save user to gsq_users table in database
	$servername = "localhost" ;
    $username = "gsqtest_gsqAdmin";
    $password = "gsqProject2016";
    $dbname = "gsqtest_users";

// Create connection
		$conn = new mysqli($servername, $username, $password, $dbname);
				// Check connection
		if ($conn->connect_error) {
				die("Connection failed: " . $conn->connect_error);
			}
		$membership_id = mysqli_real_escape_string( $conn,$membership_id);
		$email = mysqli_real_escape_string($conn,$email);
		$sql = "SELECT  membership_ID ,  join_date , years_of_renewal , renewal_untill , years_a_member , membership_class, is_direct , branch , is_single, interest_areas, find_about , surname, first_name, gender, email, birth_year,title, address1, address2, city, state, country,postcode, postal_address,home_phone, work_phone,mobile, emergency_name, emergency_number , emergency_relationship
				FROM  gsq_members , gsq_users
				WHERE gsq_members.user_ID = gsq_users.user_ID
				AND membership_ID = ? AND email = ?
		";
		if ($stmt = $conn->prepare($sql)) {
			  $stmt->bind_param("ss", $membership_id , $email);
			  $stmt->execute();
			  $stmt->store_result();
			  if ($stmt->num_rows == 0){
				     $records_found = false ;
					die("Error: No result found");
			  }else{
					$stmt->bind_result( $membership_ID ,  $join_date , $years_of_renewal , $renewal_untill , $years_a_member , $membership_class, $is_direct , $branch , $is_single, $interest_areas, $find_about , $surname, $first_name, $gender, $email, $birth_year,$title, $address1, $address2, $city, $state, $country,$postcode, $postal_address,$home_phone, $work_phone,$mobile, $emergency_name, $emergency_number , $emergency_relationship);
					$stmt->fetch();
					$stmt->close();
					$conn->close();
			  }
		}else{
			  die("Error: Query can not be executed");
		}

?>

<?php if($records_found == true){ ?>
  <form action="http://www.ozbizonline.com.au/edit-membership-details/" method="post" >
	  <fieldset class="summary">
		  <p> <label> <span id="summary2">Your details:</span></label></p> 
		  <fieldset>
				 <div class="summaryLegend">Personal Details:</div>
				 <input type="hidden" name="membership_id" value="<?php echo $membership_id; ?>" >
			    <input type="hidden" name="expiry_date" value="<?php echo $renewal_untill ; ?>" >
				 <p> <label class = "field">Surname:</label><input type="text" name="surname" value="<?php echo "$surname"; ?>" readonly></p>
				 <p> <label class = "field">Given names:</label><input type="text" name="pname" value="<?php echo "$first_name"; ?>" readonly></p>
				 <p> <label class = "field"><span>*</span>Title:</label><input type="text" name="title"  value="<?php echo "$title"; ?>" readonly></p>
				 <p> <label class = "field"><span>*</span>Gender:</label><input type="text"  name="gender" value="<?php echo "$gender"; ?>" readonly></p>
				 <p> <label class = "field"><span>*</span>Birth Year:</label><input type="text" name="yob" value="<?php echo "$birth_year"; ?>" readonly ></p>
		  </fieldset>
		  <fieldset>
				<div class="summaryLegend">Contact Details:</div>
				 <p> <label class = "field"> Adress1:</label><input type="text" name="address1"  value="<?php echo "$address1"; ?>" readonly></p>
				 <p> <label class = "field"> Adress2:</label><input type="text" name="address2"  value="<?php echo "$address2"; ?>" readonly></p>
				 <p> <label class = "field"> City/Suburb:</label><input type="text" name="city"  value="<?php echo "$city"; ?>" readonly></p>
				  <?php if($state != "other"){ ?>
						  <p> <label class = "field"> State:</label><input type="text" name="state"  value="<?php echo "$state"; ?>" readonly></p>
				  <?php } else { ?>
						  <p> <label class = "field"> State:</label><input type="text" name="state"  value="<?php echo "$stateOther"; ?>" readonly></p>
				  <?php }  ?>
				 <p> <label class = "field"> Country:</label><input type="text" name="country"  value="<?php echo "$country"; ?>"  readonly> </p>
				 <p> <label class = "field"> Post Code:</label><input type="text" id="post_code" name="post_code"  value="<?php echo "$postcode"; ?>" readonly></p>
				 <p> <label class = "field"> Postal Address:</label><input type="text" id="postal_address" name="postal_address"  value="<?php echo "$postal_address"; ?>" readonly></p>
				 <p> <label class = "field"><span>*</span> Email:</label><input type="text" name="emailID"  value="<?php echo "$email"; ?>" readonly></p>
		  </fieldset>
		  <fieldset>
			   <br>
				 <div class="summaryLegend">Contact Number:</div>
				 <p> <label class = "field"> Home tel:</label><input type="number" id="code" name="tel"  value="<?php echo "$home_phone"; ?>" readonly> </p>
				 <p> <label class = "field"> Work tel:</label><input type="number" id="work_tel" name="work_tel"  value="<?php echo "$work_phone"; ?>" readonly></p>
				 <p> <label class = "field"> Mobile:</label><input type="number" id="mobile" name="mob"  value="<?php echo "$mobile"; ?>" readonly></p>
		  </fieldset>
		  <fieldset>
		   <br>
		     <div class="summaryLegend">Emergency details:</div>
			<p> <label class = "field"> Name:</label><input type="text" id="emergency_name" name="emergency_name"  value="<?php echo "$emergency_name"; ?>" readonly> </p>
			<p> <label class = "field"> Phone number:</label><input type="number" id="emergency_number" name="emergency_number" value="<?php echo "$emergency_number"; ?>" readonly></p>
			<p> <label class = "field"> Relationship:</label><input type="text" id="emergency_relationship" name="emergency_relationship" value="<?php echo "$emergency_relationship"; ?>" readonly></p>
		  <fieldset>
				 <br>
				  <div class="summaryLegend">Membership details:</div>
				 <p>
					<?php if( $is_direct  == "true"){ ?>
						<p> <input type="text" name="membership1" value="<?php echo "Direct"; ?>" readonly></p>
					<?php } ?>
					<?php if($branch != "" || $branch != null){ ?>
						 <p> <input type="text" name="membership1" value="<?php echo "$branch"; ?>" readonly></p>
					<?php } ?>
					<?php if( $is_single == "true"){ ?>
						<p> <input type="text" name="membership2" value="<?php echo "Single"; ?>" readonly></p>
					<?php }else{ ?>
						 <p> <input type="text" name="membership2" value="<?php echo "Family"; ?>" readonly></p>
					<?php } ?>
					 <p> <label class = "field"> Membership Class:</label><input name="membership_class" value="<?php echo "$membership_class"; ?>" readonly></p>
					<?php $newDate = date("d-m-Y", strtotime($renewal_untill ));?>
					<p> <label class = "field"> Membership Expires On: </label><input name="membership_expires" value="<?php echo "$newDate"; ?>" readonly></p>
				   <input type="hidden" name="membership_duration" value="<?php echo "$years_of_renewal"; ?>" readonly></p>
				   <input type="hidden" name="join_date" value="<?php echo $join_date ; ?>" readonly></p>
					
				 </p>
			 </fieldset>
			 <fieldset>
				 <div class="summaryLegend">Interest:</div>
						 
						 <p> <label class = "field"> Interest Areas:</label><input type="text" name="interest" value="<?php echo "$interest_areas"; ?>" readonly></p>
						 
			 </fieldset>
			  
	  </fieldset>
	 
<!-- <input type="button" id="agree" value="Proceed" onClick="document.location.href='http://www.ozbizonline.com.au/?page_id=485'" />
	  <input type="button" id="agreeNot" value="Edit Detail"  onClick="document.location.href='http://www.ozbizonline.com.au/edit-membership-details/'" /> -->
	  <button type="submit" id="agree" name="Proceed" formaction="http://www.ozbizonline.com.au/save-membership/">Proceed</button>
      <button type="submit" id="agreeNot" name="Edit" formaction="http://www.ozbizonline.com.au/edit-membership-details/">Edit Details</button>
</form>
    
<?php }else{ 
  echo "<meta http-equiv='refresh' content='0;url=http://www.ozbizonline.com.au/?page_id=497'>";
exit();
 } ?>



<?php
/*
$goBack = <<<GOBACK
<form action="http://www.ozbizonline.com.au/?page_id=300" method="post">   
    <input type="Submit" value="Go Back">
</form>
GOBACK;
$goBack2 = <<<GOBACK2
<form action="http://www.ozbizonline.com.au/?page_id=300" method="post">   
    <input type="Submit" value="Confirm Detail">
</form>
GOBACK2;
echo $goBack;
echo $goBack2;
*/
?>


<?php	while ( have_posts() ) {
		weaverx_post_count_clear(); the_post();

		get_template_part( 'templates/content', 'page' );

		comments_template( '', true );
	}

	weaverx_sb_postcontent('page');

	weaverx_page_tail( 'page', $sb_layout );    // end of page wrap
?>