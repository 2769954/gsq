
<?php
// Start the session
session_start();
?>
<?php
//phpinfo();
error_reporting(E_ALL);
ini_set( 'display_errors','1');
?>
<?php
/*
Template Name: save unchanged membership renewal details to database
*/
?>
<?php
//error handler function
function customError($errno, $errstr) {
  echo "<b>Error:</b> [$errno] $errstr";
}

//set error handler
set_error_handler("customError");

?>
<?php
if ( !defined('ABSPATH')) exit; // Exit if accessed directly

// this file updates membership ,if the member has not changed any details, in database
?>
<?php	$sb_layout = weaverx_page_lead( 'page' ); ?>
<?php	weaverx_sb_precontent('page');?>
 
 <?php
    $membership_id = $membership_duration="";
    $error = 'no';
    if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        if (empty($_POST["membership_id"])) {
            $error= 'membership_id';
        } else {
            $membership_id = test_input($_POST["membership_id"]);
        }
        
        if (empty($_POST["membership_duration"])) {
         }else
         {
            $membership_duration = test_input($_POST["membership_duration"]);
            
        }
		
		if (empty($_POST["expiry_date"])) {
            $error= 'expiry_date';
        } else {
            $expiry_date = test_input($_POST["expiry_date"]);
        }
		$join_date = test_input($_POST["join_date"]);
   }
   

   // validate all input data for illegell characters
function test_input($data) {
   $data = trim($data);
   $data = substr($data, 0, 500);
   $data = stripslashes($data);
   $data = htmlspecialchars($data);
   $data = EscapeShellCmd($data);
   return $data;
}

 if($membership_id == "" || $membership_id == null )
 {
     die("An error occurred");
 }else
 {
         $servername = "localhost" ;
         $username = "gsqtest_gsqAdmin";
         $password = "gsqProject2016";
         $dbname = "gsqtest_users";
         $conn = new mysqli($servername, $username, $password, $dbname);
				// Check connection
        if ($conn->connect_error)
        {
				die("Connection failed: " . $conn->connect_error);
		} 
		$membership_id = mysqli_real_escape_string($conn, $membership_id);
        
        //renew membership save today's date in gsq_members table
       // $join_date = date("Y-m-d H:i:s"); // today's date
	   $todays_date =time();
	   $expiry_date =  strtotime($expiry_date );
	   $difference  = $todays_date-$expiry_date;
	   $difference = floor($difference / (60 * 60 * 24)); // calculate number of days
	   // check for grace period
	   if($difference < 60){
			  $renewal_untill2 = date("Y-m-d H:i:s",strtotime("+$membership_duration years",  $expiry_date));
	   }else{
		
			  $years_of_renewal = $membership_duration;
			  $renewal_untill=strtotime("$years_of_renewal Years");
			  $renewal_untill2 = date("Y-m-d H:i:s", $renewal_untill);
	   }
		  // find since how many years a member
	    $diff2 =  strtotime(date("Y-m-d H:i:s")) - strtotime($join_date);
		$diff2= floor($diff2 / (60 * 60 * 24*365)); // calculate number of days
    	$years_a_member = $diff2;
        $sql= " UPDATE `gsq_members` SET `renewal_untill`=?,`years_a_member`=?
                WHERE membership_id = ?";
			if ($stmt = $conn->prepare($sql))
            {
                    $stmt->bind_param("sdd",$renewal_untill2,$years_a_member,$membership_id);
					$stmt->execute(); 
                    if ($stmt->errno) {
                            die ("FAILURE!!! " . $stmt->error);
                    }
                    $stmt->close();
                    echo "<p>Membership renewed succefully</p>";
            }else
            {
                echo "Error: " . $sql . "<br>" . $conn->error;
            }
 }

 ?>


<?php	while ( have_posts() ) {
		weaverx_post_count_clear(); the_post();

		get_template_part( 'templates/content', 'page' );

		comments_template( '', true );
	}

	weaverx_sb_postcontent('page');

	weaverx_page_tail( 'page', $sb_layout );    // end of page wrap
?>