
<?php
// Start the session
session_start();
?>
<?php
/*
Template Name: become volunteer page 2
*/
?>

<?php
//error handler function
function customError($errno, $errstr) {
  echo "<b>Error:</b> [$errno] $errstr";
}

//set error handler
set_error_handler("customError");
?>

<?php
if ( !defined('ABSPATH')) exit; // Exit if accessed directly
/**
 * Standard page output (Default template)
 */
?>
<?php	$sb_layout = weaverx_page_lead( 'page' ); ?>
<?php	weaverx_sb_precontent('page');?>


 <?php
    $membership_id = $userID =  $email =  $postcodeErr = $emailErr= "";
    $surname = $is_direct =$is_single = $firstname = $title = $birth_year="";
    $membership_id= $branch = $membership_class= $home_phone= $work_phone="";
    $mobile=$interest_areas=$enews = $yob = $email = $gender = $branch ="";
    $membership2 = $address1= $address2 = $state = $work_tel = $mob = $tel ="";
    $condition = $telCode = $includeMe = $pname = $suburb = $postcode = $confirmEmail = "";
    $surnameFamily = $forenameFamily = $titleFamily = $yobFamily = $pnameFamily = $mobFamily = $genderFamily= "";
	$records_found = true ;		// if records of membership with given membeship_id and emailid do exist
	
	
     if ($_SERVER["REQUEST_METHOD"] == "POST") {
       $membership_id = test_input($_POST["membership_id"]);
	   $email = test_input($_POST["email"]);
	}
    
    // validate all input data for illegell characters
    function test_input($data) {
      $data = trim($data);
      $data = stripslashes($data);
      $data = htmlspecialchars($data);
      return $data;
    }		
	// save user to gsq_users table in database
	$servername = "localhost" ;
    $username = "gsqtest_gsqAdmin";
    $password = "gsqProject2016";
    $dbname = "gsqtest_users";

// Create connection
		$conn = new mysqli($servername, $username, $password, $dbname);
				// Check connection
		if ($conn->connect_error) {
				die("Connection failed: " . $conn->connect_error);
			}
		$membership_id = mysqli_real_escape_string( $conn,$membership_id);
		$email = mysqli_real_escape_string($conn,$email);
		$sql = "SELECT gsq_users.user_ID, membership_ID ,  join_date , years_of_renewal , renewal_untill , years_a_member , membership_class, is_direct , branch , is_single, interest_areas, find_about , surname, first_name, gender, email, birth_year,title, address1, address2, city, state, country,postcode, postal_address,home_phone, work_phone,mobile, emergency_name, emergency_number , emergency_relationship
				FROM  gsq_members , gsq_users
				WHERE gsq_members.user_ID = gsq_users.user_ID
				AND membership_ID = ? AND email = ?
		";
		if ($stmt = $conn->prepare($sql)) {
			  $stmt->bind_param("ss", $membership_id , $email);
			  $stmt->execute();
			  $stmt->store_result();
			  if ($stmt->num_rows == 0){
				     $records_found = false ;
					die("Error: No result found");
			  }else{
					$stmt->bind_result( $userID,$membership_ID ,  $join_date , $years_of_renewal , $renewal_untill , $years_a_member , $membership_class, $is_direct , $branch , $is_single, $interest_areas, $find_about , $surname, $first_name, $gender, $email, $birth_year,$title, $address1, $address2, $city, $state, $country,$postcode, $postal_address,$home_phone, $work_phone,$mobile, $emergency_name, $emergency_number , $emergency_relationship);
					$stmt->fetch();
					$stmt->close();
					$conn->close();
			  }
		}else{
			  die("Error: Query can not be executed");
		}

?>


<?php if($records_found == true){ ?>

 <h3>Following is the summary of your profile:</h3>
	
  <form>
	<!--  <fieldset class="summary">
		  <p> <label> <span id="summary2">Your details:</span></label></p> -->
		  <fieldset>
				 <div class="summaryLegend">Personal Details:</div>
				 <input type="hidden" name="membership_id" value="<?php echo $membership_id; ?>" >
        
				 <p> <label class = "field">Surname:</label><input type="text" name="surname" value="<?php echo "$surname"; ?>" readonly></p>
				 <p> <label class = "field">Given names:</label><input type="text" name="pname" value="<?php echo "$first_name"; ?>" readonly></p>
				 <p> <label class = "field">Title:</label><input type="text" name="title"  value="<?php echo "$title"; ?>" readonly></p>
				 <p> <label class = "field">Gender:</label><input type="text"  name="gender" value="<?php echo "$gender"; ?>" readonly></p>
				 <p> <label class = "field">Birth Year:</label><input type="text" name="yob" value="<?php echo "$birth_year"; ?>" readonly ></p>
		  </fieldset>
		  <fieldset>
				<div class="summaryLegend">Contact Details:</div>
				 <p> <label class = "field"> Adress1:</label><input type="text" name="address1"  value="<?php echo "$address1"; ?>" readonly></p>
				 <p> <label class = "field"> Adress2:</label><input type="text" name="address2"  value="<?php echo "$address2"; ?>" readonly></p>
				 <p> <label class = "field"> City/Suburb:</label><input type="text" name="city"  value="<?php echo "$city"; ?>" readonly></p>
				  <?php if($state != "other"){ ?>
						  <p> <label class = "field"> State:</label><input type="text" name="state"  value="<?php echo "$state"; ?>" readonly></p>
				  <?php } else { ?>
						  <p> <label class = "field"> State:</label><input type="text" name="state"  value="<?php echo "$stateOther"; ?>" readonly></p>
				  <?php }  ?>
				 <p> <label class = "field"> Country:</label><input type="text" name="country"  value="<?php echo "$country"; ?>"  readonly> </p>
				 <p> <label class = "field"> Post Code:</label><input type="text" id="post_code" name="post_code"  value="<?php echo "$postcode"; ?>" readonly></p>
				 <p> <label class = "field"> Postal Address:</label><input type="text" id="postal_address" name="postal_address"  value="<?php echo "$postal_address"; ?>" readonly></p>
				 <p> <label class = "field"><span>*</span> Email:</label><input type="text" name="emailID"  value="<?php echo "$email"; ?>" readonly></p>
		  </fieldset>
		  <fieldset>
			   <br>
				 <div class="summaryLegend">Contact Number:</div>
				 <p> <label class = "field"> Home tel:</label><input type="number" id="code" name="tel"  value="<?php echo "$home_phone"; ?>" readonly> </p>
				 <p> <label class = "field"> Work tel:</label><input type="number" id="work_tel" name="work_tel"  value="<?php echo "$work_phone"; ?>" readonly></p>
				 <p> <label class = "field"> Mobile:</label><input type="number" id="mobile" name="mob"  value="<?php echo "$mobile"; ?>" readonly></p>
		  </fieldset>
		  <fieldset>
		   <br>
		     <div class="summaryLegend">Emergency details:</div>
			<p> <label class = "field"> Name:</label><input type="text" id="emergency_name" name="emergency_name"  value="<?php echo "$emergency_name"; ?>" readonly> </p>
			<p> <label class = "field"> Phone number:</label><input type="number" id="emergency_number" name="emergency_number" value="<?php echo "$emergency_number"; ?>" readonly></p>
			<p> <label class = "field"> Relationship:</label><input type="text" id="emergency_relationship" name="emergency_relationship" value="<?php echo "$emergency_relationship"; ?>" readonly></p>
		   </fieldset>
          <fieldset>
				 <br>
				  <div class="summaryLegend">Membership details:</div>
				 <p>
					<?php if( $is_direct  == "true"){ ?>
						<p> <input type="text" name="membership1" value="<?php echo "Direct"; ?>" readonly></p>
					<?php } ?>
					<?php if($branch != "" || $branch != null){ ?>
						 <p> <input type="text" name="membership1" value="<?php echo "$branch"; ?>" readonly></p>
					<?php } ?>
					<?php if( $is_single == "true"){ ?>
						<p> <input type="text" name="membership2" value="<?php echo "Single"; ?>" readonly></p>
					<?php }else{ ?>
						 <p> <input type="text" name="membership2" value="<?php echo "Family"; ?>" readonly></p>
					<?php } ?>
					 <p> <label class = "field"> Membership Class:</label><input name="membership_class" value="<?php echo "$membership_class"; ?>" readonly></p>
					<?php $newDate = date("d-m-Y", strtotime($renewal_untill ));?>
					<p> <label class = "field"> Membership Expires On: </label><input name="membership_expires" value="<?php echo "$newDate"; ?>" readonly></p>
				    <p> <label class = "field"> Membership Duration: </label><input name=" years_of_renewal" value="<?php echo " $years_of_renewal"; ?>" readonly></p>
					
				 </p>
			 </fieldset>
			 <fieldset>
				 <div class="summaryLegend">Interest:</div>
						 
						 <p> <label class = "field"> Interest Areas:</label><input type="text" name="interest" value="<?php echo "$interest_areas"; ?>" readonly></p>
						 
			 </fieldset>
</form>
   <h3>Please select the volunteering roles from the form below and submit: </h3>
   <form method="post" id="my_form" action="http://www.ozbizonline.com.au/save-volunteer/">
	    <input type="hidden" name="user_id" value="<?php echo $userID; ?>" >
            <table class="volunteer">
                <tr>
                    <th>
                       Area
                    </th>
                    <th>
                       Brief Description
                    </th>
                    <th>
                       
                    </th>
                </tr>
                <tr>
                    <th colspan="3">
                       Publications & Groups
                    </th>
                </tr>
                <tr>
                    <td>
                       Editorial Committee
                    </td>
                    <td>
                       Assist in a range of tasks associated with the production and distribution of our journal Generation.
                    </td>
                    <td>
                       <input type="checkbox" name="editorial_Committee" value="Editorial Committee">
                    </td>
                </tr>
                  <tr>
                    <td>
                     Interest Groups
                    </td>
                    <td>
                       Be involved in coordination, newsletter production and group assistance.
                    </td>
                    <td>
                       <input type="checkbox" name="interest_Groups" value="Interest Groups">
                    </td>
                </tr>
                <tr>
                    <td>
                    Proof reading
                    </td>
                    <td>
                       Proofing documents eg indexing, eNews, Journal & web site material
                    </td>
                    <td>
                       <input type="checkbox" name="proof_reading" value="Proof reading">
                    </td>
                </tr>
                 <tr>
                    <th colspan="3">
                      Events & Activities
                    </th>
                </tr>
                <tr>
                    <td>
                     Education & Speakers
                    </td>
                    <td>
                       Conduct classes or seminars on various aspects of genealogy. Speak at public events, group meetings etc
                    </td>
                    <td>
                       <input type="checkbox" name="education_Speakers" value="Education & Speakers">
                    </td>
                </tr>
                  <tr>
                    <td>
                    Event Catering
                    </td>
                    <td>
                       Organisation/assisting with setup of catering.
                    </td>
                    <td>
                       <input type="checkbox" name="event_Catering" value="Event Catering">
                    </td>
                </tr>
                   <tr>
                        <td>
                         Library Acquisitions
                        </td>
                        <td>
                          Be involved in making decisions on the type of records to be purchased and maintain existing records.
                        </td>
                        <td>
                           <input type="checkbox" name="library_Acquisitions" value="Library Acquisitions">
                        </td>
                    </tr>
                    <tr>
                        <td>
                         Finance/Bookkeeping
                        </td>
                        <td>
                          Assist the GSQ Treasurer with various duties.
                        </td>
                        <td>
                           <input type="checkbox" name="finance_Bookkeeping" value="Finance/Bookkeeping">
                        </td>
                   </tr>
                     <tr>
                        <td>
                        Membership
                        </td>
                        <td>
                          Assist the Membership Officer in processing memberships & maintaining the membership database.
                        </td>
                        <td>
                           <input type="checkbox" name="membership" value="Membership">
                        </td>
                   </tr>
                   <tr>
                        <td>
                       Secretarial
                        </td>
                        <td>
                          Assist GSQ Secretary with administration tasks.
                        </td>
                        <td>
                           <input type="checkbox" name="secretarial" value="Secretarial">
                        </td>
                   </tr>
                   <tr>
                        <td>
                      Publicity
                        </td>
                        <td>
                          Publicise GSQ and its various events. Can you provide media contacts?.
                        </td>
                        <td>
                           <input type="checkbox" name="publicity" value="Publicity">
                        </td>
                   </tr>
                   <tr>
                        <td>
                      Social Media
                        </td>
                        <td>
                          Research & collection of articles of genealogical interest to be shared with the membership via social media.
                        </td>
                        <td>
                           <input type="checkbox" name="social_Media" value="Social Media">
                        </td>
                   </tr>
                   <tr>
                        <th colspan="3">
                          Information Technology/computer related
                        </th>
                    </tr>
                    <tr>
                        <td>
                        Computer training 
                        </td>
                        <td>
                          Experience conducting training in software packages eg Word, Excel, Publisher etc.
                        </td>
                        <td>
                           <input type="checkbox" name="computer_training" value="Computer Training">
                        </td>
                   </tr>
                    <tr>
                        <td>
                        Graphic Design
                        </td>
                        <td>
                         Design skills using professional software for publications, brochures, flyers and the web site.
                        </td>
                        <td>
                           <input type="checkbox" name="graphic_Design" value="Graphic Design">
                        </td>
                   </tr>
                    <tr>
                        <td>
                        General IT
                        </td>
                        <td>
                         Able to apply updates to Windows software, rectify issues etc as part of a technical team.
                        </td>
                        <td>
                           <input type="checkbox" name="general_IT" value="General IT">
                        </td>
                   </tr>
                    <tr>
                        <td>
                        Web site development
                        </td>
                        <td>
                         Expertise with web related software including WordPress, PHP, HTML etc 
                        </td>
                        <td>
                           <input type="checkbox" name="website_development" value="Web site development">
                        </td>
                   </tr>
                     <tr>
                        <th colspan="3">
                          Library
                        </th>
                    </tr>
                    <tr>
                        <td>
                        Library Assistant
                        </td>
                        <td>
                         As part of a team, help members with their research in the library (training provided).
                        </td>
                        <td>
                           <input type="checkbox" name="library_Assistant" value="Library Assistant">
                        </td>
                   </tr>
                     <tr>
                        <td>
                        Reception
                        </td>
                        <td>
                        Meet & greet our members – VIP Greeter.
                        </td>
                        <td>
                           <input type="checkbox" name="reception" value="reception">
                        </td>
                   </tr>
                   <tr>
                        <td>
                        Projects eg indexing, transcription
                        </td>
                        <td>
                          In the library, at home or elsewhere, indexing of records from fiche, film or hard copy.
                        </td>
                        <td>
                           <input type="checkbox" name="projects" value="Projects">
                        </td>
                   </tr>
                   <tr>
                        <td>
                       Research
                        </td>
                        <td>
                          Be part of an experienced team conducting investigations for the research service.
                        </td>
                        <td>
                           <input type="checkbox" name="research" value="research">
                        </td>
                   </tr>
                    <tr>
                        <th colspan="3">
                         Property  maintenance
                        </th>
                    </tr>
                      <tr>
                        <td>
                       Trade
                        </td>
                        <td>
                          Electrician, Carpenter, Plumber.
                        </td>
                        <td>
                           <input type="checkbox" name="trade" value="trade">
                        </td>
                   </tr>
                       <tr>
                        <td>
                         Genereal
                        </td>
                        <td>
                            Handyman, gardener.
                        </td>
                        <td>
                           <input type="checkbox" name="general" value="general">
                        </td>
                   </tr>
                     </tr>
                       <tr>
                        <td colspan="2">
                         Other skills which may be of use to GSQ
                        </td>
                        
                        <td>
                           <input type="checkbox" name="other" value="other">
                        </td>
                   </tr>
            </table>
            <input type="submit" class="sendIt" value="Submit">
   </form>
<?php }else{ 
  echo "<meta http-equiv='refresh' content='0;url=http://www.ozbizonline.com.au/?page_id=497'>";
exit();
 } ?>





<?php	while ( have_posts() ) {
		weaverx_post_count_clear(); the_post();

		get_template_part( 'templates/content', 'page' );

		comments_template( '', true );
	}

	weaverx_sb_postcontent('page');

	weaverx_page_tail( 'page', $sb_layout );    // end of page wrap
?>