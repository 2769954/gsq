
<?php
// Start the session
session_start();
?>
<?php
/*
Template Name: volunteer availability save to gsq_volunteers_availability table in database
*/
?>
<?php
//error handler function
function customError($errno, $errstr) {
  echo "<b>Error:</b> [$errno] $errstr";
}

//set error handler
set_error_handler("customError");

?>
<?php
if ( !defined('ABSPATH')) exit; // Exit if accessed directly
/**
 * This page saves the volunteer roles ,of an already existing member/user, to the database.
 */
?>
<?php	$sb_layout = weaverx_page_lead( 'page' ); ?>
<?php	weaverx_sb_precontent('page');?>
 
<?php
// define variables and set to empty values


$select = "no" ; //if no role selected

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $userID = $_POST["user_id"];

     if(isset($_POST['regularly']))
    {
            saveVolunteer($userID, $_POST['regularly']);
            $select = "yes";
     }
	   if(isset($_POST['occasionally']))
    {
            saveVolunteer($userID, $_POST['occasionally']);
            $select = "yes";
     }
	   if(isset($_POST['work_from_home']))
    {
            saveVolunteer($userID, $_POST['work_from_home']);
            $select = "yes";
     }
	    if(isset($_POST['saturdays']))
    {
            saveVolunteer($userID, $_POST['saturdays']);
            $select = "yes";
     }
	     if(isset($_POST['evenings']))
    {
            saveVolunteer($userID, $_POST['evenings']);
            $select = "yes";
     }
	      if(isset($_POST['special_Events']))
    {
            saveVolunteer($userID, $_POST['special_Events']);
            $select = "yes";
     }
        if(isset($_POST['membership']))
    {
            saveVolunteer($userID, $_POST['membership']);
            $select = "yes";
     }
	 
    if (empty($_POST['not_available'])) 
    {
    
    }else{
            saveVolunteer($userID, test_input($_POST['not_available']));
            $select = "yes";
     }
	 
}
// validate all input data for illegela characters
function test_input($data) {
  $data = trim($data);
  $data = substr($data, 0, 500);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  $data = EscapeShellCmd($data);
  return $data;
}

// save values to gsq_volunteer_roles
function saveVolunteer($userID, $role){
    $servername = "localhost" ;
    $username = "gsqtest_gsqAdmin";
    $password = "gsqProject2016";
    $dbname = "gsqtest_users";
    $conn = new mysqli($servername, $username, $password, $dbname);
	// Check connection
	if ($conn->connect_error)
    {
			die("Connection failed: " . $conn->connect_error);
	}
  
    $sql = "INSERT INTO `gsq_volunteers_availability`
                     (`user_id`, `availability`)
                     VALUES ( ?,?)";
    if ($stmt = $conn->prepare($sql))
    {
           
				$stmt->bind_param("ss",$userID,$role);
				$stmt->execute();
				$last_id = $conn->insert_id;
				if($last_id == 0){
						// error occured while saving in database
						die("An error occured");
				}else{
					$stmt->close();	
				}
				
                 
    }else{
        echo "Error: " .  $conn->error;
    }
 
}
?>
<?php
    if($select == "no")
    {
         echo '<h3>You have not selected any availabiility time.Your response has been saved.</h3>';        
    }else{
     
?>
         <p>Thank you for being a Volunteer. Your response has been saved.</p>
		
		 <br>
<?php    }
				
  
?>

<?php	while ( have_posts() ) {
		weaverx_post_count_clear(); the_post();

		get_template_part( 'templates/content', 'page' );

		comments_template( '', true );
	}

	weaverx_sb_postcontent('page');

	weaverx_page_tail( 'page', $sb_layout );    // end of page wrap
?>