
<?php
// Start the session
session_start();
?>
<?php
/*
Template Name: save vounteers to database for an existing member
*/
?>
<?php
//error handler function
function customError($errno, $errstr) {
  echo "<b>Error:</b> [$errno] $errstr";
}

//set error handler
set_error_handler("customError");

?>
<?php
if ( !defined('ABSPATH')) exit; // Exit if accessed directly
/**
 * This page saves the volunteer roles ,of an already existing member/user, to the database.
 */
?>
<?php	$sb_layout = weaverx_page_lead( 'page' ); ?>
<?php	weaverx_sb_precontent('page');?>
 
<?php
// define variables and set to empty values
$surnameErr = $pnameErr = $forenameErr = $titleErr= "";
$yobErr = $familyInfoErr = $addressErr  = $user_id = $emailErr = $branchErr ="";
$stateErr = $confirmEmailErr = $genderErr = $suburbErr = $conditionErr = $postcodeErr = $contactErr = "";
$surname = $forename = $title = $emergency_relationship=   $findUs = $membership_id = "";
$emergency_name= $membership_class = $membership_duration =$emergency_number =$enews ="";
$yob = $email = $gender = $branch = $membership2 = $address1="";
$address2 =$postal_address = $state = $work_tel = $mob = $tel = $condition = $telCode = $pname = $suburb = $postcode = $confirmEmail = "";
$surnameFamily = $forenameFamily = $titleFamily  =  $enews = $emergencyErr = $yobFamily = $pnameFamily = $mobFamily = $genderFamily= "";
$userID = $editorial_Committee ="";

$select = "no" ; //if no role selected

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $userID = $_POST["user_id"];
	if(isset($_POST['editorial_Committee']))
    {
            saveVolunteer($userID, $_POST['editorial_Committee']);
            $select = "yes";
     }
     if(isset($_POST['interest_Groups']))
    {
            saveVolunteer($userID, $_POST['interest_Groups']);
            $select = "yes";
     }
	   if(isset($_POST['proof_reading']))
    {
            saveVolunteer($userID, $_POST['proof_reading']);
            $select = "yes";
     }
	   if(isset($_POST['education_Speakers']))
    {
            saveVolunteer($userID, $_POST['education_Speakers']);
            $select = "yes";
     }
	    if(isset($_POST['event_Catering']))
    {
            saveVolunteer($userID, $_POST['event_Catering']);
            $select = "yes";
     }
	     if(isset($_POST['library_Acquisitions']))
    {
            saveVolunteer($userID, $_POST['library_Acquisitions']);
            $select = "yes";
     }
	      if(isset($_POST['finance_Bookkeeping']))
    {
            saveVolunteer($userID, $_POST['finance_Bookkeeping']);
            $select = "yes";
     }
        if(isset($_POST['membership']))
    {
            saveVolunteer($userID, $_POST['membership']);
            $select = "yes";
     }
	    if(isset($_POST['secretarial']))
    {
            saveVolunteer($userID, $_POST['secretarial']);
            $select = "yes";
     }
	 if(isset($_POST['publicity']))
    {
            saveVolunteer($userID, $_POST['publicity']);
            $select = "yes";
     }
	  if(isset($_POST['social_Media']))
    {
            saveVolunteer($userID, $_POST['social_Media']);
            $select = "yes";
     }
	  if(isset($_POST['computer_training']))
    {
            saveVolunteer($userID, $_POST['computer_training']);
            $select = "yes";
     }
	   if(isset($_POST['graphic_Design']))
    {
            saveVolunteer($userID, $_POST['graphic_Design']);
            $select = "yes";
     }
	   if(isset($_POST['general_IT']))
    {
            saveVolunteer($userID, $_POST['general_IT']);
            $select = "yes";
     }
	   if(isset($_POST['website_development']))
    {
            saveVolunteer($userID, $_POST['website_development']);
            $select = "yes";
     }
	    if(isset($_POST['library_Assistant']))
    {
            saveVolunteer($userID, $_POST['library_Assistant']);
            $select = "yes";
     }
	     if(isset($_POST['reception']))
    {
            saveVolunteer($userID, $_POST['reception']);
            $select = "yes";
     }
	      if(isset($_POST['projects']))
    {
            saveVolunteer($userID, $_POST['projects']);
            $select = "yes";
     }
	    if(isset($_POST['research']))
    {
            saveVolunteer($userID, $_POST['research']);
            $select = "yes";
     }
	    if(isset($_POST['trade']))
    {
            saveVolunteer($userID, $_POST['trade']);
            $select = "yes";
     }
	    if(isset($_POST['general']))
    {
            saveVolunteer($userID, $_POST['general']);
            $select = "yes";
     }
	    if(isset($_POST['other']))
    {
            saveVolunteer($userID, $_POST['other']);
            $select = "yes";
     }
}

// save values to gsq_volunteer_roles
function saveVolunteer($userID, $role){
    $servername = "localhost" ;
    $username = "gsqtest_gsqAdmin";
    $password = "gsqProject2016";
    $dbname = "gsqtest_users";
    $conn = new mysqli($servername, $username, $password, $dbname);
	// Check connection
	if ($conn->connect_error)
    {
			die("Connection failed: " . $conn->connect_error);
	}
  
    $sql = "INSERT INTO gsq_volunteer_roles
                     ( user_id,roles)
                     VALUES ( ?,?)";
    if ($stmt = $conn->prepare($sql))
    {
           
				$stmt->bind_param("ss",$userID,$role);
				$stmt->execute();
				$last_id = $conn->insert_id;
				if($last_id == 0){
						// error occured while saving in database
						die("An error occured");
				}else{
						//now save to the gsq_volunteers table;
						$sql2 = "INSERT INTO gsq_volunteers
								( user_id)
								VALUES (?)";
						if ($stmt = $conn->prepare($sql2))
						{
								 $stmt->bind_param("s",$userID);
								 $stmt->execute();
						}
				}
				$stmt->close();
                 
    }else{
        echo "Error: " .  $conn->error;
    }
 
}
?>
<?php
    if($select == "no")
    {
         echo '<h3>You have not selected any volunteer role. Please go back and select a role if you want to  be a Volunteer.</h3>';        
    }else{
       //$conn->close();
	    echo '<h3> Availability.</h3>';
?>      <p>Please select your availability</p>
		 <form method="post" action="http://www.ozbizonline.com.au/volunteers-availability/">
				 <input type="hidden" name="user_id" value="<?php echo $userID; ?>" >
				 <input type="checkbox" name="regularly" value="regularly">   Regularly<br>
                 <input type="checkbox" name="occasionally" value="occasionally"> Occasionally <br>
                  <input type="checkbox" name="work_from_home" value=" Work from home"> Work from home <br>
                  <input type="checkbox" name="week_days" value="Week days"> Week days <br>
                <input type="checkbox" name="saturdays" value="Saturdays">  Saturdays  <br>
                <input type="checkbox" name="evenings" value="Evenings"> Evenings <br>
                <input type="checkbox" name="special_Events" value="Special Events"> Special Events <br>
                <label> Please indicate any times when you are NOT available</label>  <input type="text" name="not_available">
				  <input type="submit" class="sendIt" value="Submit">
        </form>
		 <br>
<?php    }
				
  
?>

<?php	while ( have_posts() ) {
		weaverx_post_count_clear(); the_post();

		get_template_part( 'templates/content', 'page' );

		comments_template( '', true );
	}

	weaverx_sb_postcontent('page');

	weaverx_page_tail( 'page', $sb_layout );    // end of page wrap
?>