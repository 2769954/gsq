
<?php
// Start the session
session_start();
// This file validates input information for a non existing volunteer and saves to the gsq_users table in the database
?>
<?php
/*
Template Name: validate and save to database the user details of non-existing volunteer
*/
?>

<?php
if ( !defined('ABSPATH')) exit; // Exit if accessed directly
/**
 * Standard page output (Default template)
 */
?>
<?php	$sb_layout = weaverx_page_lead( 'page' ); ?>
<?php	weaverx_sb_precontent('page');?>
 <?php

			$country = $_SESSION['country'];
			$discount =  $_SESSION["discount"];
?>
<?php
// define variables and set to empty values
$surnameErr = $pnameErr = $forenameErr = $titleErr= $yobErr = $familyInfoErr = $addressErr = $emailErr = $branchErr = $stateErr = $confirmEmailErr = $genderErr = $suburbErr = $conditionErr = $postcodeErr = $contactErr = "";
$surname = $forename = $title = $emergency_relationship= $emergency_name= $membership_class = $membership_duration =$emergency_number =$enews = $yob = $email = $gender = $branch = $membership2 = $address1= $address2 =$postal_address = $state = $work_tel = $mob = $tel = $condition = $telCode = $pname = $suburb = $postcode = $confirmEmail = "";
$surnameFamily = $forenameFamily = $titleFamily  =  $enews = $emergencyErr = $yobFamily = $pnameFamily = $mobFamily = $genderFamily= "";
$submit = "correct" ;

if ($_SERVER["REQUEST_METHOD"] == "POST") {
   
 if (empty($_POST["surname"])) {
    $surnameErr = "* Name is required";
	$submit = "surname" ;
	
  } else {
    $surname = test_input($_POST["surname"]);
	$_SESSION['surname'] = $surname;
    // check if name only contains letters and whitespace
    if (!preg_match("/^[a-zA-Z ]*$/",$surname)) {
      $surnameErr = "* Only letters and white space allowed in name";
	  $submit = "surname" ;
    }
  }
  
   if (empty($_POST["state"])) {
    $state= "";
	$submit = "state" ;
  } else {
    $state = test_input($_POST["state"]);
	$stateOther = test_input($_POST["stateOther"]);
	$_SESSION['state'] = $state;
	$_SESSION['stateOther'] = $stateOther;
	if ($state == "other" && empty($_POST["stateOther"])){
		   $stateErr = "You have not selected the name of your state";
		   $submit = "stateOther" ;
	}
	
  }
  
  if (empty($_POST["forename"])) {
    $forenameErr = " * forename is required";
	$submit = "forename" ;
  } else {
    $forename = test_input($_POST["forename"]);
	$_SESSION['forename'] = $forename;
    // check if name only contains letters and whitespace
    if (!preg_match("/^[a-zA-Z ]*$/",$forename)) {
      $forenameErr = "*Only letters and white space allowed";
	  $submit = "foreneme" ;
    }
  }

  if (empty($_POST["pname"])) {

    }
   else {
    $pname = test_input($_POST["pname"]);
    $_SESSION['pname'] = $pname;
	 if (!preg_match("/^[a-zA-Z ]*$/",$pname)) {
      $pnameErr = " * Only letters and white space allowed";
	  $submit = "pname" ;
		   }
    }

   if (empty($_POST["gender"])) {
    $genderErr = "*Gender is required";
	$submit = "gender" ;
  } else {
    $gender = test_input($_POST["gender"]);
    $_SESSION['gender'] = $gender;
  }
  
  if (empty($_POST["emergency_name"])) {
    $emergencyErr = "*Emergency name is required";
	$submit = "emergencyName" ;
  } else {
    $emergency_name = test_input($_POST["emergency_name"]);
    $_SESSION['emergency_name'] = $emergency_name;
  }
   if (empty($_POST["emergency_number"])) {
    $emergencyErr = "*Emergency number is required";
	$submit = "emergencyNumber" ;
  } else {
    $emergency_number = test_input($_POST["emergency_number"]);
    $_SESSION['emergency_number'] = $emergency_number;
  }
   if (empty($_POST["emergency_relationship"])) {
    $emergencyErr = "*Emergency contact relationship is required";
	$submit = "emergencyRelation" ;
  } else {
    $emergency_relationship = test_input($_POST["emergency_relationship"]);
    $_SESSION['emergency_relationship'] = $emergency_relationship;
  }
     if (empty($_POST["condition"])) {
    $conditionErr = "* You must agree with GSQ terms and conditions";
	$submit = "condition" ;
  } else {
    $condition = test_input($_POST["condition"]);
    $_SESSION['condition'] = $condition;
  }

   if (empty($_POST["title"])) {
    $titleErr = "* Title is required";
	$submit = "title" ;
  } else {
    $title = test_input($_POST["title"]);
    $_SESSION['title'] = $title;
	if($title == "oth"){
		   $_SESSION['title_other'] = test_input($_POST["title_other"]);;
	}
  }

   if (empty($_POST["birthyear"])) {
    $yobErr = "* Birth year is required";
	$submit = "yob" ;
  } else {
    $yob = test_input($_POST["birthyear"]);
    $_SESSION['yob'] = $yob;
  }
  
    if (empty($_POST["address1"]) && empty($_POST["address2"])) {
    $addressErr = "*address is required";
	$submit = "address";
  } else {
    $address1 = test_input($_POST["address1"]);
    $_SESSION['address1'] = $address1;
	$address2 = test_input($_POST["address2"]);
	$_SESSION['address2'] = $address2;
  }
   
  if (empty($_POST["suburb"])) {
    $suburbErr = "* suburb is required";
	$submit = "suburb";
  } else {
    $suburb = test_input($_POST["suburb"]);
	$_SESSION['suburb'] = $suburb;
  }

  if (empty($_POST["post_code"])) {
    $postcodeErr = "* postcode is required";
	$submit = "postcode";
  } else {
    $postcode = test_input($_POST["post_code"]);
	$_SESSION['postcode'] = $postcode;
	$numLength = strlen($postcode);
    // check if post code is a number and length is equal to 4 
		   if (($numLength != 4) || (!is_numeric($postcode))) {
				$postcodeErr = "* postcode is not valid";
				$postcode = "";
				$submit = "postcode";
		   }else{
		       // validate the post codes for indvidual states
		       validate_state_post_code($state,$postcode,$postcodeErr);	  
		   } 
  }

  if (empty($_POST["emailID"])) {
    $emailErr = "* Email is required";
	$submit = "email";
  } else {
    $email = test_input($_POST["emailID"]);
	$_SESSION['email'] = $email;
    // check if e-mail address is well-formed
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $emailErr = "* Invalid email format";
	  $submit = "email";
    }
  }

  if ((empty($_POST["confirm_email"])) || ($_POST["confirm_email"] != $email) ) {
    $confirmEmailErr = "* Emails do not match";
	$submit = "confirm_email";
  } else {
    $confirmEmail = test_input($_POST["confirm_email"]);
  }

   if ((empty($_POST["tel"])) && (empty($_POST["mob"])) && (empty($_POST["work_tel"])) ) {
   //if (empty($_POST["mob"])) {
    $contactErr = "* You must Enter a contact number";
	$submit = "contactNumber";
  } else {
    $tel = test_input($_POST["tel"]);
	$_SESSION['tel'] = $tel;
	$work_tel = test_input($_POST["work_tel"]);
	$_SESSION['work_tel'] = $work_tel;
	$mob = test_input($_POST["mob"]);
	$_SESSION['mob'] = $mob;
   }

  if (empty($_POST["find_us"])) {
    $find_us = "";
  } else {
		   $findUs = test_input($_POST["find_us"]);
		   $_SESSION['findUs'] = $findUs;
		   if($findUs == "oth_findus"){
				 $_SESSION['findUs'] =  test_input($_POST["findus_other"]);	  
		   }
		   
    }
    
  if (empty($_POST["interest"])) {
    $interest= "";
  } else {
    $interest = test_input($_POST["interest"]);
	$_SESSION['interest'] = $interest;
  }
    if (empty($_POST["postal_address"])) {
   
    $postal_address= "";
  } else {
   $postal_address = test_input($_POST["postal_address"]);
	$_SESSION['postal_address'] =   $postal_address;
  }
   if (empty($_POST["membership_class"])) {
    $membership_class = "";
  } else {
   $membership_class = test_input($_POST["membership_class"]);
	$_SESSION['membership_class'] =   $membership_class;
  }
  
  
}

// validate all input data for illegell characters
function test_input($data) {
   $data = trim($data);
   $data = substr($data, 0, 500);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  $data = EscapeShellCmd($data);
  return $data;
}

//function to validate post codes for a state
function validate_state_post_code($state, $postcode,$postcodeErr ){
   if(($state == "nsw" && $postcode[0] != "2") ||
	  ($state == "nt" &&  $postcode[0] != "0")  ||
        ($state == "qld" && $postcode[0] != "4") ||
        ($state == "sa" && $postcode[0] != "5")  ||
        ($state == "tas" && $postcode[0] != "7") ||
        ($state == "vic" && $postcode[0] != "3") ||
        ($state == "wa" && $postcode[0] != "6") 
	  ){
		    $GLOBALS['postcodeErr'] = "* Post code for the seleted state is not right";
			$postcodeErr = "* Post code for the seleted state is not right";
	         $submit = "postCodeState";
   }else{

   }
}
//action="<?php echo htmlspecialchars($_SERVER["REQUEST_URI"]);"
?>

<?php
$html = <<<HTML
<form name="myForm" method="post" >
<fieldset>
<legend>Please go back and correct following errors</legend>
<span class="error"> $surnameErr</span>
<span class="error"> $pnameErr </span>
<br>
<span class="error"> $titleErr</span>
<br>
<span class="error"> $genderErr </span>
<br>
<span class="error"> $yobErr </span>
<br>
<span class="error">$addressErr</span>
<br>
<span class="error">$suburbErr</span>
<br>
<span class="error"> $stateErr</span>
<br>
<span class="error"> $postcodeErr</span>
<br>
<br>
<span class="error"> $emergencyErr</span>
<br>
<span class="error"> $branchErr</span>
<br>
<span class="error"> $emailErr </span>
<br>
<span class="error">$confirmEmailErr</span>
<br>
<span class="error">$conditionErr</span>
<br>
<span class="error">$familyInfoErr</span>
</fieldset>
</form>
HTML;
?>


<?php if($submit != "correct"){ 
//echo $submit; 
echo $html ;
$goBack = <<<GOBACK
<form action="http://www.ozbizonline.com.au/?page_id=300" method="post">   
    <input type="Submit" value="Go Back">
</form>
GOBACK;
echo $goBack;
} else{
  
	 // save user to gsq_users table in database
	 $servername = "localhost" ;
    $username = "gsqtest_gsqAdmin";
    $password = "gsqProject2016";
    $dbname = "gsqtest_users";

// Create connection
			$conn = new mysqli($servername, $username, $password, $dbname);
				// Check connection
			if ($conn->connect_error) {
				die("Connection failed: " . $conn->connect_error);
			} 
			$surname = mysqli_real_escape_string($conn, $surname);
			$forename = mysqli_real_escape_string($conn, $forename);
			$email = mysqli_real_escape_string($conn, $email);
			$yob = mysqli_real_escape_string($conn,$yob);
			$address1 = mysqli_real_escape_string($conn, $address1);
			$address2 = mysqli_real_escape_string($conn, $address2);
			$suburb = mysqli_real_escape_string($conn, $suburb);
			$state= mysqli_real_escape_string($conn, $state);
			$country = mysqli_real_escape_string($conn, $country);
			$postcode = mysqli_real_escape_string($conn, $postcode);
			$tel = mysqli_real_escape_string($conn,$tel);
			$work_tel = mysqli_real_escape_string($conn, $work_tel);
			$mob = mysqli_real_escape_string($conn, $mob);
			$title = mysqli_real_escape_string($conn, $title);
			$findUs= mysqli_real_escape_string($conn, $findUs);
			$interest = mysqli_real_escape_string($conn, $interest);
			$postal_address = mysqli_real_escape_string($conn, $postal_address);
			$emergency_name = mysqli_real_escape_string($conn,$emergency_name);
			$emergency_number = mysqli_real_escape_string($conn,$emergency_number);
			$emergency_relationship = mysqli_real_escape_string($conn,$emergency_relationship);
		
			if($country != "Australia"){
			  $state = "" ;
			}
		    if($surname == "" || $surname == null || $forename == "" || $forename == null || $email == "" || $email == null){
			   unset($_SESSION['surnamename']);
			   session_unset();
			  session_destroy();
			  //if user directly opens this page
			   die("Error: An error occured, incomplete information");
			}
			$sql = "INSERT INTO gsq_users
					( surname, first_name, gender,email,birth_year,address1,address2,city,state,country,postcode,postal_address,home_phone,work_phone,mobile,title,emergency_name,emergency_number,emergency_relationship )
					 VALUES ( ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			 // Attempt to prepare the query
			 if ($stmt = $conn->prepare($sql))
			 {
						$stmt->bind_param("sssssssssssssssssss", $surname , $forename , $gender , $email, $yob, $address1, $address2 , $suburb,$state,$country,$postcode,$postal_address, $tel ,$work_tel ,$mob , $title,$emergency_name,$emergency_number,$emergency_relationship);
						$stmt->execute();
						$last_id = $conn->insert_id;
						if($last_id == 0){
							unset($_SESSION['surnamename']);
							session_unset();
							session_destroy();
							// error occured while saving in database probabl reason unique email id constraint
							 die("An error occured");
						}
						 
						 $stmt->close();
						 echo '<h3>Your details are successfully added to the system.<br> </h3>';
						 ?>
						 <p></p>
						 <h3>Please Select the volunteer roles from the table below and press submit.</h3>
						 <form method="post" id="my_form" action="http://www.ozbizonline.com.au/save-volunteer-roles/">
							  <input type="hidden" name="user_id" value="<?php echo $last_id; ?>" >
								  <table class="volunteer">
									  <tr>
										  <th>
											 Area
										  </th>
										  <th>
											 Brief Description
										  </th>
										  <th>
											 
										  </th>
									  </tr>
									  <tr>
										  <th colspan="3">
											 Publications & Groups
										  </th>
									  </tr>
									  <tr>
										  <td>
											 Editorial Committee
										  </td>
										  <td>
											 Assist in a range of tasks associated with the production and distribution of our journal Generation.
										  </td>
										  <td>
											 <input type="checkbox" name="editorial_Committee" value="Editorial Committee">
										  </td>
									  </tr>
										<tr>
										  <td>
										   Interest Groups
										  </td>
										  <td>
											 Be involved in coordination, newsletter production and group assistance.
										  </td>
										  <td>
											 <input type="checkbox" name="interest_Groups" value="Interest Groups">
										  </td>
									  </tr>
									  <tr>
										  <td>
										  Proof reading
										  </td>
										  <td>
											 Proofing documents eg indexing, eNews, Journal & web site material
										  </td>
										  <td>
											 <input type="checkbox" name="proof_reading" value="Proof reading">
										  </td>
									  </tr>
									   <tr>
										  <th colspan="3">
											Events & Activities
										  </th>
									  </tr>
									  <tr>
										  <td>
										   Education & Speakers
										  </td>
										  <td>
											 Conduct classes or seminars on various aspects of genealogy. Speak at public events, group meetings etc
										  </td>
										  <td>
											 <input type="checkbox" name="education_Speakers" value="Education & Speakers">
										  </td>
									  </tr>
										<tr>
										  <td>
										  Event Catering
										  </td>
										  <td>
											 Organisation/assisting with setup of catering.
										  </td>
										  <td>
											 <input type="checkbox" name="event_Catering" value="Event Catering">
										  </td>
									  </tr>
										 <tr>
											  <td>
											   Library Acquisitions
											  </td>
											  <td>
												Be involved in making decisions on the type of records to be purchased and maintain existing records.
											  </td>
											  <td>
												 <input type="checkbox" name="library_Acquisitions" value="Library Acquisitions">
											  </td>
										  </tr>
										  <tr>
											  <td>
											   Finance/Bookkeeping
											  </td>
											  <td>
												Assist the GSQ Treasurer with various duties.
											  </td>
											  <td>
												 <input type="checkbox" name="finance_Bookkeeping" value="Finance/Bookkeeping">
											  </td>
										 </tr>
										   <tr>
											  <td>
											  Membership
											  </td>
											  <td>
												Assist the Membership Officer in processing memberships & maintaining the membership database.
											  </td>
											  <td>
												 <input type="checkbox" name="membership" value="Membership">
											  </td>
										 </tr>
										 <tr>
											  <td>
											 Secretarial
											  </td>
											  <td>
												Assist GSQ Secretary with administration tasks.
											  </td>
											  <td>
												 <input type="checkbox" name="secretarial" value="Secretarial">
											  </td>
										 </tr>
										 <tr>
											  <td>
											Publicity
											  </td>
											  <td>
												Publicise GSQ and its various events. Can you provide media contacts?.
											  </td>
											  <td>
												 <input type="checkbox" name="publicity" value="Publicity">
											  </td>
										 </tr>
										 <tr>
											  <td>
											Social Media
											  </td>
											  <td>
												Research & collection of articles of genealogical interest to be shared with the membership via social media.
											  </td>
											  <td>
												 <input type="checkbox" name="social_Media" value="Social Media">
											  </td>
										 </tr>
										 <tr>
											  <th colspan="3">
												Information Technology/computer related
											  </th>
										  </tr>
										  <tr>
											  <td>
											  Computer training 
											  </td>
											  <td>
												Experience conducting training in software packages eg Word, Excel, Publisher etc.
											  </td>
											  <td>
												 <input type="checkbox" name="computer_training" value="Computer Training">
											  </td>
										 </tr>
										  <tr>
											  <td>
											  Graphic Design
											  </td>
											  <td>
											   Design skills using professional software for publications, brochures, flyers and the web site.
											  </td>
											  <td>
												 <input type="checkbox" name="graphic_Design" value="Graphic Design">
											  </td>
										 </tr>
										  <tr>
											  <td>
											  General IT
											  </td>
											  <td>
											   Able to apply updates to Windows software, rectify issues etc as part of a technical team.
											  </td>
											  <td>
												 <input type="checkbox" name="general_IT" value="General IT">
											  </td>
										 </tr>
										  <tr>
											  <td>
											  Web site development
											  </td>
											  <td>
											   Expertise with web related software including WordPress, PHP, HTML etc 
											  </td>
											  <td>
												 <input type="checkbox" name="website_development" value="Web site development">
											  </td>
										 </tr>
										   <tr>
											  <th colspan="3">
												Library
											  </th>
										  </tr>
										  <tr>
											  <td>
											  Library Assistant
											  </td>
											  <td>
											   As part of a team, help members with their research in the library (training provided).
											  </td>
											  <td>
												 <input type="checkbox" name="library_Assistant" value="Library Assistant">
											  </td>
										 </tr>
										   <tr>
											  <td>
											  Reception
											  </td>
											  <td>
											  Meet & greet our members – VIP Greeter.
											  </td>
											  <td>
												 <input type="checkbox" name="reception" value="reception">
											  </td>
										 </tr>
										 <tr>
											  <td>
											  Projects eg indexing, transcription
											  </td>
											  <td>
												In the library, at home or elsewhere, indexing of records from fiche, film or hard copy.
											  </td>
											  <td>
												 <input type="checkbox" name="projects" value="Projects">
											  </td>
										 </tr>
										 <tr>
											  <td>
											 Research
											  </td>
											  <td>
												Be part of an experienced team conducting investigations for the research service.
											  </td>
											  <td>
												 <input type="checkbox" name="research" value="research">
											  </td>
										 </tr>
										  <tr>
											  <th colspan="3">
											   Property  maintenance
											  </th>
										  </tr>
											<tr>
											  <td>
											 Trade
											  </td>
											  <td>
												Electrician, Carpenter, Plumber.
											  </td>
											  <td>
												 <input type="checkbox" name="trade" value="trade">
											  </td>
										 </tr>
											 <tr>
											  <td>
											   Genereal
											  </td>
											  <td>
												  Handyman, gardener.
											  </td>
											  <td>
												 <input type="checkbox" name="general" value="general">
											  </td>
										 </tr>
										   </tr>
											 <tr>
											  <td colspan="2">
											   Other skills which may be of use to GSQ
											  </td>
											  
											  <td>
												 <input type="checkbox" name="other" value="other">
											  </td>
										 </tr>
								  </table>
								  <input type="submit" class="sendIt" value="Submit">
						 </form>
<?php	 	} else {	
			     echo "Error: " . $sql . "<br>" . $conn->error;
			}
//echo "<meta http-equiv='refresh' content='0;url=http://www.ozbizonline.com.au/?page_id=375'>";
exit();

  }
?>

<?php	while ( have_posts() ) {
		weaverx_post_count_clear(); the_post();

		get_template_part( 'templates/content', 'page' );

		comments_template( '', true );
	}

	weaverx_sb_postcontent('page');

	weaverx_page_tail( 'page', $sb_layout );    // end of page wrap
?>