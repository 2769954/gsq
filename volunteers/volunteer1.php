<?php
//phpinfo();
error_reporting(E_ALL);
ini_set( 'display_errors','1');
?>
<?php
// Start the session
session_start();
?>
<?php
/*
Template Name: become volunteer first page
*/
?>

<?php
if ( !defined('ABSPATH')) exit; // Exit if accessed directly
/**
 * Standard page output (Default template)
 */
?>
<?php	$sb_layout = weaverx_page_lead( 'page' ); ?>
<?php	weaverx_sb_precontent('page');?>
<?php	while ( have_posts() ) {
		weaverx_post_count_clear(); the_post();

		get_template_part( 'templates/content', 'page' );

		comments_template( '', true );
	}

	weaverx_sb_postcontent('page');?>
 
 <p>Already a member? Please enter your email and membership id </p>
<form id="myGsqLogin" name="membershipForm1" onsubmit="return validateForm3()" action="http://www.ozbizonline.com.au/volunteering-for-existing-members/" method="post" >
 <div class="login" >
       
            <input type="text" name="email" id="email" placeholder="Email Id">
            <p id="emailID"></p>
            <input type="password" name="membership_id" id="membership_id" placeholder="Membership Id">
            <p id="areaCode"></p>
            <input type="submit" value="Submit">
      </div>
  
  </form>
<p></p>
<p>Not a member? Click <a href="http://www.ozbizonline.com.au/volunteering-for-non-existing-members/">here</a> to become a volunteer</p>


<?php	weaverx_page_tail( 'page', $sb_layout );    // end of page wrap
?>