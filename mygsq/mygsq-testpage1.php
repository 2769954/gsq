
<?php
// Start the session
session_start();
?>
<?php
/*
Template Name: mygsq test page 1
*/
?>
<?php
if ( !defined('ABSPATH')) exit; // Exit if accessed directly
/**
 * Standard page output (Default template)
 */
?>
<?php	$sb_layout = weaverx_page_lead( 'page' ); ?>
<?php	weaverx_sb_precontent('page');?>
 
<?php
	if (isset($_SESSION['message'])) {
		echo "<div id='error_msg'>".$_SESSION['message']."</div>";
		unset($_SESSION['message']);
	}
?>
<?php // sesion expires after 30 minutes of inactivity
		if( $_SESSION['last_activity'] < time()-$_SESSION['expire_time'] ) { //have we expired?
			session_destroy();
			unset($_SESSION['username']);
			unset($_SESSION['loggedIn']);
			unset( $_SESSION['renewal_date'] );
			unset( $_SESSION['membershipID']);
			unset(	$_SESSION['firstName']);
			$_SESSION['message'] = "You are now logged out";
		} else{ //if we haven't expired:
			$_SESSION['last_activity'] = time(); //this was the moment of last activity.
		}
?>
<!-- Check authorisation of the user  -->
<?php if( !isset( $_SESSION['username']) ) {  // not logged in?> 
			you need to login to view this content.
			<div><a href="http://mygsq.ozbizonline.com.au/mygsq-login/">Login</a></div>
<?php }else{ // logged in  ?> 
			<div class="logout-menu"><a href="http://mygsq.ozbizonline.com.au/mygsq-logout/">Logout  </a>
					<?php echo $_SESSION['username'];?>
			</div>
			<?php
				$expiry_date = $_SESSION['renewal_date'];
				$newDate = date("d-m-Y", strtotime($expiry_date ));
				 $todays_date = date("d-m-Y H:i:s");;
			?>
	        <!-- Check if membership is not expired  -->
			<?php if (strtotime($_SESSION['renewal_date']) < strtotime($todays_date)){ // membership is expired ?>  
				   <h4>Hello  <?php echo  $_SESSION['firstName']; ?></h4>
				   <p>Your membership expired on :	 <?php echo($newDate); ?></p>
				   <a href="http://www.ozbizonline.com.au/renew-membership/">Renew Membership </a>
			 <?php }else{ // membership is not expired ?>
					<div><h4>Welcome <?php echo $_SESSION['username'];  ?></h4></div>
					<p> You should see this content only if you are logged in and your membership is current </p>
					<p> There should be an option of logout on top right corner</p>
			<?php } ?>
<?php } ?>

<?php	while ( have_posts() ) {
		weaverx_post_count_clear(); the_post();

		get_template_part( 'templates/content', 'page' );

		comments_template( '', true );
	}

	weaverx_sb_postcontent('page');

	weaverx_page_tail( 'page', $sb_layout );    // end of page wrap
?>