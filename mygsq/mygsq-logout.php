<?php
/*
//phpinfo();
error_reporting(E_ALL);
ini_set( 'display_errors','1');
*/
?>
<?php
// Start the session
session_start();
?>
<?php
/*
Template Name: mygsq logout page
*/
?>

<?php
/*
//error handler function
function customError($errno, $errstr) {
  echo "<b>Error:</b> [$errno] $errstr";
}

//set error handler
set_error_handler("customError");
*/
?>
<?php
if ( !defined('ABSPATH')) exit; // Exit if accessed directly
/**
 * Standard page output (Default template)
 */
?>
<?php	$sb_layout = weaverx_page_lead( 'page' ); ?>
<?php	weaverx_sb_precontent('page');?>
 
<?php
	session_destroy();
	unset($_SESSION['username']);
	unset($_SESSION['loggedIn']);
	//$_SESSION['message'] = "You are now logged out";
   echo "<h3>You have successfully logged out </h3>";
	//header("location: login.php");
    //echo "<meta http-equiv='refresh' content='0;url= http://mygsq.ozbizonline.com.au/mygsq-login/'>";
?>
<?php	while ( have_posts() ) {
		weaverx_post_count_clear(); the_post();

		get_template_part( 'templates/content', 'page' );

		comments_template( '', true );
	}

	weaverx_sb_postcontent('page');

	weaverx_page_tail( 'page', $sb_layout );    // end of page wrap
?>