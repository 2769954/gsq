<?php
//phpinfo();
error_reporting(E_ALL);
ini_set( 'display_errors','1');
?>
<?php
// Start the session
session_start();
?>
<?php
/*
Template Name: save user to database
*/
?>

<?php
//error handler function
function customError($errno, $errstr) {
  echo "<b>Error:</b> [$errno] $errstr";
}

//set error handler
set_error_handler("customError");

?>
<?php
if ( !defined('ABSPATH')) exit; // Exit if accessed directly
/**
 * Standard page output (Default template)
 */
?>
<?php	$sb_layout = weaverx_page_lead( 'page' ); ?>
<?php	weaverx_sb_precontent('page');?>
 <?php
    $surname = $forename = $title = $enews = $findUs= $interest= $country= $yob = $membership_class =$emergency_relationship= $emergency_name=$emergency_number= $email = $gender = $branch  = $membership2 = $address1= $address2 = $postal_address = $state = $work_tel = $mob = $tel = $condition = $telCode = $includeMe = $pname = $suburb = $postcode = $confirmEmail = "";
    $surnameFamily = $forenameFamily  = $titleFamily = $yobFamily = $pnameFamily = $mobFamily = $genderFamily= "";
			
			 if (isset($_SESSION['country'])){
                $country  = $_SESSION['country'];                 
            }
			
			 if (isset($_SESSION['discount'])){
                $discount =  $_SESSION["discount"];                 
            }
            
			 if (isset($_SESSION['surname'])){
                $surname  =  $_SESSION["surname"];                
            }
         
			 if (isset($_SESSION['forename'])){
                   $forename =  $_SESSION["forename"];                
            }
           
			 if (isset($_SESSION['pname'])){
                 $pname    =  $_SESSION["pname"];              
            }
         
			 if (isset($_SESSION['title'])){
                   $title    =  $_SESSION["title"];             
            }
            
			 if (isset($_SESSION['gender'])){
               $gender   =  $_SESSION["gender"];            
            }
           
			 if (isset($_SESSION['yob'])){
               $yob      =  $_SESSION["yob"];           
            }
           if (isset($_SESSION['emergency_name'])){
                    $emergency_name =  $_SESSION['emergency_name'] ;  
            }
			 if (isset($_SESSION['emergency_number'])){
                    $emergency_number =  $_SESSION['emergency_number'] ;  
            }
			 if (isset($_SESSION['emergency_relationship'])){
                    $emergency_relationship =  $_SESSION['emergency_relationship'] ;  
            }
			if (isset($_SESSION['address1'])){
                $address1 =  $_SESSION["address1"];          
            }
          
			if (isset($_SESSION['address2'])){
                 $address2 =  $_SESSION["address2"];         
            }
			if (isset($_SESSION['postal_address'])){
                 $postal_address =  $_SESSION["postal_address"];         
            }
            
			if (isset($_SESSION['suburb'])){
               $suburb   =  $_SESSION["suburb"];         
            }
           
			if (isset($_SESSION['postcode'])){
                $postcode =  $_SESSION["postcode"];        
            }
           
			if (isset($_SESSION['state'])){
                $state    =  $_SESSION["state"];       
            }
          
			if (isset($_SESSION['email'])){
                $email    =  $_SESSION["email"];      
            }
           
			if (isset($_SESSION['telCode'])){
                $telCode  =  $_SESSION['telCode'];     
            }
        
			if (isset($_SESSION['tel'])){
                $tel =  $_SESSION['tel'] ;    
            }
           
			if (isset($_SESSION['work_tel'])){
                 $work_tel =  $_SESSION['work_tel'] ;   
            }
        
			if (isset($_SESSION['mob'])){
                    $mob =  $_SESSION['mob'] ;  
            }
           
			if (isset($_SESSION['membership'])){
                    $membership = $_SESSION['membership'] ;  
            }
            if (isset($_SESSION['membership2'])){
                    $membership2 = $_SESSION['membership2'] ;  
            }
			if (isset($_SESSION['membership_class'])){
                    $membership_class = $_SESSION['membership_class'] ;  
            }
			if (isset($_SESSION['findUs'])){
                  $findUs   =  $_SESSION['findUs']; 
            }
          
			if (isset($_SESSION['interest'])){
                 $interest =  $_SESSION['interest'];
            }
          
		   if (isset( $_SESSION['membership_duration'])){
                $membership_duration =  $_SESSION['membership_duration'];
            }
		   if (isset($_SESSION['branchName2'])){
               $branch =  $_SESSION['branchName2'];
            }
          if (isset($_SESSION['enews'])){
                 $includeMe =  $_SESSION['enews'];                 
            }
             if (isset($_SESSION['surnameFamily'])){
                 $surnameFamily = $_SESSION['surnameFamily'];                  
            }
            if (isset($_SESSION['fornameFamily'])){
                $fornameFamily = $_SESSION['fornameFamily'];               
            }
            if (isset($_SESSION['pnameFamily'])){
                $pnameFamily =  $_SESSION['pnameFamily'];              
            }
             if (isset($_SESSION['titleFamily'])){
                $titleFamily =  $_SESSION['titleFamily'];              
            }
            if (isset($_SESSION['yobFamily'])){
                $yobFamily  = $_SESSION['yobFamily'];             
            }
            if (isset($_SESSION['mobFamily'])){
                 $mobFamily =  $_SESSION['mobFamily'];            
            }
             if (isset($_SESSION['genderFamily'])){
               $genderFamily = $_SESSION['genderFamily'];          
            }
              if (isset($_SESSION['stateOther'])){
               $stateOther= $_SESSION['stateOther'];          
            }
              if (isset($_SESSION['branchName2'])){
               $branchName2 = $_SESSION['branchName2'];          
            }
            
?>


<?php
     // save user to gsq_users table in database
	 $servername = "localhost" ;
    $username = "gsqtest_gsqAdmin";
    $password = "gsqProject2016";
    $dbname = "gsqtest_users";

// Create connection
			$conn = new mysqli($servername, $username, $password, $dbname);
				// Check connection
			if ($conn->connect_error) {
				die("Connection failed: " . $conn->connect_error);
			} 
			$surname = mysqli_real_escape_string($conn, $surname);
			$forename = mysqli_real_escape_string($conn, $forename);
			$email = mysqli_real_escape_string($conn, $email);
			$yob = mysqli_real_escape_string($conn,$yob);
			$address1 = mysqli_real_escape_string($conn, $address1);
			$address2 = mysqli_real_escape_string($conn, $address2);
			$suburb = mysqli_real_escape_string($conn, $suburb);
			$state= mysqli_real_escape_string($conn, $state);
			$country = mysqli_real_escape_string($conn, $country);
			$postcode = mysqli_real_escape_string($conn, $postcode);
			$tel = mysqli_real_escape_string($conn,$tel);
			$work_tel = mysqli_real_escape_string($conn, $work_tel);
			$mob = mysqli_real_escape_string($conn, $mob);
			$title = mysqli_real_escape_string($conn, $title);
			$findUs= mysqli_real_escape_string($conn, $findUs);
			$interest = mysqli_real_escape_string($conn, $interest);
			$postal_address = mysqli_real_escape_string($conn, $postal_address);
			$membership_duration = mysqli_real_escape_string($conn,$membership_duration);
			$membership_class = mysqli_real_escape_string($conn,$membership_class);
			$emergency_name = mysqli_real_escape_string($conn,$emergency_name);
			$emergency_number = mysqli_real_escape_string($conn,$emergency_number);
			$emergency_relationship = mysqli_real_escape_string($conn,$emergency_relationship);
			if($country != "Australia"){
			  $state = "" ;
			}
		    if($surname == "" || $surname == null || $forename == "" || $forename == null || $email == "" || $email == null){
			   unset($_SESSION['surnamename']);
			   session_unset();
			  session_destroy();
			  //if user directly opens this page
			   die("Error: An error occured, incomplete information");
			}
			$sql = "INSERT INTO gsq_users
					( surname, first_name, gender,email,birth_year,address1,address2,city,state,country,postcode,postal_address,home_phone,work_phone,mobile,title,emergency_name,emergency_number,emergency_relationship )
					 VALUES ( ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			 // Attempt to prepare the query
			 if ($stmt = $conn->prepare($sql)) {
				   $stmt->bind_param("sssssssssssssssssss", $surname , $forename , $gender , $email, $yob, $address1, $address2 , $suburb,$state,$country,$postcode,$postal_address, $tel ,$work_tel ,$mob , $title,$emergency_name,$emergency_number,$emergency_relationship);
				   $stmt->execute();
				   $last_id = $conn->insert_id;
				   if($last_id == 0){
					unset($_SESSION['surnamename']);
				    session_unset();
				    session_destroy();
					// error occured while saving in database probabl reason unique email id constraint
					 die("An error occured");
				   }
					
				    $stmt->close();
				  // now save to gsq_members table in the same database
				  $user_id =  $last_id; //id of last inserted user
				  $membership_id = rand(100000,999999);
				  $join_date = date("Y-m-d H:i:s"); // today's date
				  $years_of_renewal = $membership_duration;
				  $renewal_untill=strtotime("$years_of_renewal Years");
				  $renewal_untill2 = date("Y-m-d H:i:s", $renewal_untill);
				  $years_a_member = 0;
  
				  if($membership == "direct"){
					$is_direct = "true" ;
				  }else{
					$is_direct = "false" ;
				  }
				  if($membership2 == "family"){
					$is_single = "false" ;
				  }else if($membership2 == "single"){
					$is_single = "true" ;
				  }else if($membership2 == "life"){
					// life enduring must be for single and for whole life of the member
					 $is_single = "true" ;
					 $years_of_renewal = 200 ;
					 $renewal_untill=strtotime("200 Years");
				     $renewal_untill2 = date("Y-m-d H:i:s", $renewal_untill);
				  }else{}
				  $sql = "INSERT INTO gsq_members
						( user_ID,membership_ID,join_date,years_of_renewal,renewal_untill,years_a_member,membership_class,is_direct,branch,is_single,interest_areas,find_about )
						VALUES ( ?,?,?,?,?,?,?, ?, ?, ?, ?,?)";
						 // Attempt to prepare the query
						if ($stmt = $conn->prepare($sql)) {
							   $stmt->bind_param("ssssssssssss",$user_id,  $membership_id, $join_date,$years_of_renewal,$renewal_untill2,$years_a_member,$membership_class,$is_direct,$branch,$is_single,$interest,$findUs	);
							   $stmt->execute();
				               $stmt->close();
						}else{
							 echo "Error: " . $sql . "<br>" . $conn->error;
						}
				  $conn->close();
				  // clear all session variables
				  unset($_SESSION['surnamename']);
				 session_unset();
				 session_destroy();
			} else {	
			     echo "Error: " . $sql . "<br>" . $conn->error;
			}
	/* global $wpdb;
	 $wpdb->show_errors = true;
	 $tel2 = strval($telCode).strval($tel);
    $wpdb->query( $wpdb->prepare( 
	"
		INSERT INTO users
		( surname,first_name,gender,email,birth_year,title,address1,address2,city,state,country,postcode,postal_address,home_phone,work_phone,mobile )
		VALUES ( %s,%s,%s,%s,%s,%s, %s, %s, %s, %s, %s,%d, %s, %s, %s, %s)
	", 
        $surname, 
	    $forename, 
	    $gender,
		$email,
		$yob,
		$title,
		$address1,
		$address2,
		$suburb,
		$state,
		$country,
		$postcode,
		$postal_address,
		$tel2,
		$work_tel,
		$mob
		
) );
*/

/* add new member's details into the members table in database */
/*
  $user_id = $wpdb->insert_id; //id of last inserted user
  $membership_id = rand(1000, 9999);
  $join_date = date("Y-m-d H:i:s"); // today's data
  $years_of_renewal = 1;
  $renewal_untill=strtotime("$years_of_renewal Years");
  $renewal_untill2 = date("Y-m-d H:i:s", $renewal_untill);
  $years_a_member = 0;
  $membership_class = "ordinary";
  if($membership == "direct"){
	$is_direct = "true" ;
  }else{
	$is_direct = "false" ;
  }
  if($membership2 == "family"){
	$is_single = "false" ;
  }else{
	$is_single = "true" ;
  }
  $wpdb->query( $wpdb->prepare( 
	"
		INSERT INTO members
		( user_ID,membership_ID,join_date,years_of_renewal,renewal_untill,years_a_member,membership_class,is_direct,branch,is_single,interest_areas,find_about )
		VALUES ( %d,%d,%s,%d,%s,%d, %s, %s, %s, %s, %s,%s)
	", 
        $user_id, 
	    $membership_id, 
	    $join_date,
		$years_of_renewal,
	    $renewal_untill2,
		$years_a_member,
		$membership_class,
		$is_direct,
		$branch,
		$is_single,
		$interest,
		$findUs	
) );
 
*/
 /*$renewal = 3;
 $renewal_untill=strtotime("$renewal Years");
  echo date("d/m/Y ", $renewal_untill) . "<br>";*/
 
?>
<p>congratultations , now you are a member of GSQ </p>
<p>use following information to use MYGSQ: <br>
 <?php echo "username: $email" ;?>
 <br>
 <?php echo "password: $membership_id" ;?>
</p>
<?php	while ( have_posts() ) {
		weaverx_post_count_clear(); the_post();

		get_template_part( 'templates/content', 'page' );

		comments_template( '', true );
	}

	weaverx_sb_postcontent('page');

	weaverx_page_tail( 'page', $sb_layout );    // end of page wrap
?>