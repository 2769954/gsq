<?php
/*
//phpinfo();
error_reporting(E_ALL);
ini_set( 'display_errors','1');
*/
?>
<?php
// Start the session
session_start();
?>
<?php
/*
Template Name: mygsq login page only
*/
?>

<?php
/*
//error handler function
function customError($errno, $errstr) {
  echo "<b>Error:</b> [$errno] $errstr";
}

//set error handler
set_error_handler("customError");
*/
?>
<?php
if ( !defined('ABSPATH')) exit; // Exit if accessed directly
/**
 * Standard page output (Default template)
 */
?>
<?php	$sb_layout = weaverx_page_lead( 'page' ); ?>
<?php	weaverx_sb_precontent('page');?>
 
<?php 

	if (isset($_POST['login_btn'])) {
      
        $servername = "localhost" ;
        $username = "gsqtest_gsqAdmin";
        $password = "gsqProject2016";
        $dbname = "gsqtest_users";

         // Create connection
		$db = new mysqli($servername, $username, $password, $dbname);
				// Check connection
		if ($db->connect_error) {
				die("Connection failed: " . $db->connect_error);
		} 
		$username = mysqli_real_escape_string($db,$_POST['username']);
		$password = mysqli_real_escape_string($db,$_POST['password']);

		$sql = "SELECT email,membership_ID,renewal_untill,first_name
		        FROM gsq_members,gsq_users
		        WHERE gsq_users.user_ID = gsq_members.user_ID AND gsq_users.email = ? AND gsq_members.membership_ID = ? ";
       if ($stmt = $db->prepare($sql)) {
					  $stmt->bind_param("ss", $username , $password);
					  $stmt->execute();
					   /* store result */
		            $stmt->store_result();
					 if ($stmt->num_rows == 0) {
								 $_SESSION['message'] = "Username/password combination incorrect";
					  }else{
								  /* bind result variables */
								 $stmt->bind_result($username,$password,$renewal_date,$firstName);
								   /* fetch value */
								 $stmt->fetch();
								 $_SESSION['username'] = $username;
								 $_SESSION['renewal_date'] = $renewal_date;
								 $_SESSION['membershipID'] = $password;
								 $_SESSION['firstName'] = $firstName;
								 $_SESSION['last_activity'] = time(); //your last activity was now, having logged in.
								 $_SESSION['expire_time'] = 1800;
								 $_SESSION['login_time'] = time();
								 $_SESSION['loggedIn'] = 1;
								  $stmt->close();
								  $db->close();
								 //echo "<meta http-equiv='refresh' content='0;url=http://mygsq.ozbizonline.com.au/mygsq-test-page-1/'>"; //redirect to home page						  
					 }
		   }else{
		      die("Error: an error occured");
		   }
	
	}
?>


<?php if( !isset( $_SESSION['username'] )) {  // Not logged in
		   if (isset($_SESSION['message'])) {
			   echo "<div id='error_msg'>".$_SESSION['message']."</div>";
			   unset($_SESSION['message']);
		   }?>
	<div class="login" >
        <form id="myGsqLogin" name="mygsq" action="http://mygsq.ozbizonline.com.au/mygsq-login/"  method="post" >
              <input type="text" name="username" id="usernameMyGSQ"  placeholder="Email Id">
              <input type="text" name="password" id="passwordMyGSQ" placeholder="Membership Id">
             <input type="submit" name="login_btn" value="Login">
       </form>
		<p></p>
		<p></p>
    </div>
<?php }else{ ?> 
		<?php
			$expiry_date = $_SESSION['renewal_date'];
			$newDate = date("d-m-Y", strtotime($expiry_date ));
			 $todays_date = date("d-m-Y H:i:s");;
		?>
		<?php
		// check if the membership has expired or not
			if (strtotime($_SESSION['renewal_date']) < strtotime($todays_date)){  	?>
			   <div class="logout-menu"><a href="http://mygsq.ozbizonline.com.au/mygsq-logout/">Logout  </a>
				<?php echo $_SESSION['username'];?>
				</div>
			   <h4>Hello  <?php echo  $_SESSION['firstName']; ?></h4>
			   <p>Your membership expired on :	 <?php echo($newDate); ?></p>
			   <a href="http://www.ozbizonline.com.au/renew-membership/">Renew Membership </a>
		 <?php }else{ // membership is not expired ?>
				<div class="logout-menu"><a href="http://mygsq.ozbizonline.com.au/mygsq-logout/">Logout  </a>
				<?php echo $_SESSION['username'];?>
				</div>
				<h4>Welcome  <?php echo  $_SESSION['firstName']; ?></h4>
			<!--	<p>Your Membership ID is : <?php //echo   $_SESSION['membershipID']; ?></p>
				<p>Your membership wil expire on : <?php //echo $newDate ; ?> </p> -->
		<?php } ?>
<?php } ?>


<?php	while ( have_posts() ) {
		weaverx_post_count_clear(); the_post();

		get_template_part( 'templates/content', 'page' );

		comments_template( '', true );
	}

	weaverx_sb_postcontent('page');

	weaverx_page_tail( 'page', $sb_layout );    // end of page wrap
?>