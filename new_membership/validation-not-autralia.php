<?php
//phpinfo();
error_reporting(E_ALL);
ini_set( 'display_errors','1');
?>
<?php
// Start the session
session_start();
?>
<?php
/*
Template Name: validate input registration other than australia
*/
?>

<?php
//error handler function
function customError($errno, $errstr) {
  echo "<b>Error:</b> [$errno] $errstr";
}

//set error handler
set_error_handler("customError");

?>
<?php
if ( !defined('ABSPATH')) exit; // Exit if accessed directly
/**
 * Standard page output (Default template)
 */
?>
<?php	$sb_layout = weaverx_page_lead( 'page' ); ?>
<?php	weaverx_sb_precontent('page');?>
 <?php

			$country = $_SESSION['country'];
			$discount =  $_SESSION["discount"];
?>
<?php
// define variables and set to empty values
$surnameErr = $pnameErr = $forenameErr = $titleErr= $yobErr = $familyInfoErr = $addressErr = $emailErr = $branchErr = $stateErr = $confirmEmailErr = $genderErr = $suburbErr = $conditionErr = $postcodeErr = $contactErr = "";
$surname = $forename = $title = $enews = $yob = $email = $gender=$membership_class = $membership_duration= $emergency_relationship= $emergency_name= $emergency_number = $branch = $address1= $address2=  $postal_address = $state = $work_tel = $mob = $tel = $condition = $telCode = $pname = $suburb = $postcode = $confirmEmail = "";
$surnameFamily = $forenameFamily = $titleFamily =  $membership2 =  $enews = $yobFamily = $pnameFamily = $mobFamily = $genderFamily= "";
$submit = "correct" ;

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if(empty($_POST["membership"])){
		   	$submit = "membership";
		  
       }else{
		    $branch = test_input($_POST["membership"]);
			$branchName2 = test_input($_POST["branchName2"]);
		    $_SESSION['membership'] = $branch;
			 $_SESSION['branchName2'] = $branchName2;
		     if( ($branch=="branch") && empty($_POST["branchName2"])) {
					$branchErr = "* You must select a branch name for your branch membership";
					$submit = "membership";
		     }
			
	   }
	   
   if(empty($_POST["membership2"])){
		   	$submit = "membership2";
		  
       }else{
		     $membership2 = test_input($_POST["membership2"]);
		     $_SESSION['membership2'] = $membership2;
					  if( $membership2 == "family"){
							  if(empty($_POST["surnameFamily"]) || empty($_POST["titleFamily"]) || empty($_POST["genderFamily"]) || empty($_POST["yobFamily"]) ){
							   $familyInfoErr = "* Family member's information is missing";
							   $submit = $submit = "familiymembership";;
							  }else{
								$surnameFamily = test_input($_POST["surnameFamily"]);
								$_SESSION['surnameFamily'] = $surnameFamily;
								$fornameFamily = test_input($_POST["foreNameFamily"]);
								$_SESSION['fornameFamily'] = $fornameFamily;
								$pnameFamily = test_input($_POST["preferNameFamily"]);
								$_SESSION['pnameFamily'] = $pnameFamily;
								$titleFamily = test_input($_POST["titleFamily"]);
								 $_SESSION['titleFamily'] = $titleFamily ;
								$yobFamily = test_input($_POST["yobFamily"]);
								 $_SESSION['yobFamily'] = $yobFamily ;
								$mobFamily = test_input($_POST["mobFamily"]);
								$_SESSION['mobFamily'] = $mobFamily ;
								 $genderFamily = test_input($_POST["genderFamily"]);
								 $_SESSION['genderFamily'] = $genderFamily  ;
							  }
					  }
		   }
  if (empty($_POST["surname"])) {
    $surnameErr = "* Name is required";
	$submit = "surname" ;
	
  } else {
    $surname = test_input($_POST["surname"]);
	$_SESSION['surname'] = $surname;
    // check if name only contains letters and whitespace
    if (!preg_match("/^[a-zA-Z ]*$/",$surname)) {
      $surnameErr = "* Only letters and white space allowed";
	  $submit = "surname" ;
    }
  }
  

  
  if (empty($_POST["forename"])) {
    $forenameErr = " * forename is required";
	$submit = "forename" ;
  } else {
    $forename = test_input($_POST["forename"]);
	$_SESSION['forename'] = $forename;
    // check if name only contains letters and whitespace
    if (!preg_match("/^[a-zA-Z ]*$/",$forename)) {
      $forenameErr = "*Only letters and white space allowed";
	  $submit = "foreneme" ;
    }
  }

  if (empty($_POST["pname"])) {

    }
   else {
    $pname = test_input($_POST["pname"]);
    $_SESSION['pname'] = $pname;
	 if (!preg_match("/^[a-zA-Z ]*$/",$pname)) {
      $pnameErr = " * Only letters and white space allowed";
	  $submit = "pname" ;
		   }
    }

   if (empty($_POST["gender"])) {
    $genderErr = "*Gender is required";
	$submit = "gender" ;
  } else {
    $gender = test_input($_POST["gender"]);
    $_SESSION['gender'] = $gender;
  }
     if (empty($_POST["condition"])) {
    $conditionErr = "* You must agree with GSQ terms and conditions";
	$submit = "condition" ;
  } else {
    $condition = test_input($_POST["condition"]);
    $_SESSION['condition'] = $condition;
  }

 if (empty($_POST["title"])) {
    $titleErr = "* Title is required";
	$submit = "title" ;
  } else {
    $title = test_input($_POST["title"]);
    $_SESSION['title'] = $title;
	if($title == "oth"){
		   $_SESSION['title_other'] = test_input($_POST["title_other"]);;
	}
  }

   if (empty($_POST["birthyear"])) {
    $yobErr = "* Birth year is required";
	$submit = "yob" ;
  } else {
    $yob = test_input($_POST["birthyear"]);
    $_SESSION['yob'] = $yob;
  }
  
    if (empty($_POST["address1"]) && empty($_POST["address2"])) {
    $addressErr = "*address is required";
	$submit = "address";
  } else {
    $address1 = test_input($_POST["address1"]);
    $_SESSION['address1'] = $address1;
	$address2 = test_input($_POST["address2"]);
	$_SESSION['address2'] = $address2;
  }
   
  if (empty($_POST["suburb"])) {
    $suburbErr = "* suburb is required";
	$submit = "suburb";
  } else {
    $suburb = test_input($_POST["suburb"]);
	$_SESSION['suburb'] = $suburb;
  }

  if (empty($_POST["post_code"])) {
    $postcodeErr = "* postcode is required";
	$submit = "postcode";
  } else {
    $postcode = test_input($_POST["post_code"]);
	$_SESSION['postcode'] = $postcode;
	$numLength = strlen($postcode);
    // check if post code is a number and length is equal to 4 
		   if  (!is_numeric($postcode)) {
				$postcodeErr = "* postcode is not valid";
				$postcode = "";
				$submit = "postcode";
		   }else{
		       // validate the post codes for indvidual states
		        
		   } 
  }
   if (empty($_POST["emergency_name"])) {
    $emergencyErr = "*Emergency name is required";
	$submit = "emergencyName" ;
  } else {
    $emergency_name = test_input($_POST["emergency_name"]);
    $_SESSION['emergency_name'] = $emergency_name;
  }
   if (empty($_POST["emergency_number"])) {
    $emergencyErr = "*Emergency number is required";
	$submit = "emergencyNumber" ;
  } else {
    $emergency_number = test_input($_POST["emergency_number"]);
    $_SESSION['emergency_number'] = $emergency_number;
  }
   if (empty($_POST["emergency_relationship"])) {
    $emergencyErr = "*Emergency contact relationship is required";
	$submit = "emergencyRelation" ;
  } else {
    $emergency_relationship = test_input($_POST["emergency_relationship"]);
    $_SESSION['emergency_relationship'] = $emergency_relationship;
  }
  if (empty($_POST["emailID"])) {
    $emailErr = "* Email is required";
	$submit = "email";
  } else {
    $email = test_input($_POST["emailID"]);
	$_SESSION['email'] = $email;
    // check if e-mail address is well-formed
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $emailErr = "* Invalid email format";
	  $submit = "email";
    }
  }

  if ((empty($_POST["confirm_email"])) || ($_POST["confirm_email"] != $email) ) {
    $confirmEmailErr = "* Emails do not match";
	$submit = "confirm_email";
  } else {
    $confirmEmail = test_input($_POST["confirm_email"]);
  }

   if ((empty($_POST["tel"])) && (empty($_POST["mob"])) && (empty($_POST["work_tel"])) ) {
    $contactErr = "* You must Enter a contact number";
	$submit = "contactNumber";
  } else {
    $tel = test_input($_POST["tel"]);
	$_SESSION['tel'] = $tel;
	$work_tel = test_input($_POST["work_tel"]);
	$_SESSION['work_tel'] = $work_tel;
	$mob = test_input($_POST["mob"]);
	$_SESSION['mob'] = $mob;

   }

  if (empty($_POST["find_us"])) {
	  $find_us = "";
  } else {
		   $findUs = test_input($_POST["find_us"]);
		   $_SESSION['findUs'] = $findUs;
		   if($findUs == "oth_findus"){
				 $_SESSION['findUs'] =  test_input($_POST["findus_other"]);	  
		   }
		   
    }

  if (empty($_POST["interest"])) {
    $interest= "";
  } else {
    $interest = test_input($_POST["interest"]);
	$_SESSION['interest'] = $interest;
  }
  
     if (empty($_POST["postal_address"])) {
    echo("empty postal address");
    $postal_address= "";
  } else {
   $postal_address = test_input($_POST["postal_address"]);
	$_SESSION['postal_address'] =   $postal_address;
  }
  if (empty($_POST["membership_class"])) {
    $membership_class = "";
  } else {
   $membership_class = test_input($_POST["membership_class"]);
	$_SESSION['membership_class'] =   $membership_class;
  }
  
  if (empty($_POST["membership_duration"])) {
    
  } else {
    $membership_duration = test_input($_POST["membership_duration"]);
	$_SESSION['membership_duration'] = $membership_duration;
  }
  if (empty($_POST["enews"])) {
    $interest= "";
  } else {
    $enews = test_input($_POST["enews"]);
	$_SESSION['enews'] = $enews;
  }
  
}

// validate all input data for illegela characters
function test_input($data) {
  $data = trim($data);
  $data = substr($data, 0, 500);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  $data = EscapeShellCmd($data);
  return $data;
}

//function to validate post codes for a state
function validate_state_post_code($state, $postcode,$postcodeErr ){
   if(($state == "nsw" && $postcode[0] != "2") ||
	  ($state == "nt" &&  $postcode[0] != "0")  ||
        ($state == "qld" && $postcode[0] != "4") ||
        ($state == "sa" && $postcode[0] != "5")  ||
        ($state == "tas" && $postcode[0] != "7") ||
        ($state == "vic" && $postcode[0] != "3") ||
        ($state == "wa" && $postcode[0] != "6") 
	  ){
		    $GLOBALS['postcodeErr'] = "* Post code for the seleted state is not right";
			$postcodeErr = "* Post code for the seleted state is not right";
	         $submit = "postCodeState";
   }else{

   }
}
//action="<?php echo htmlspecialchars($_SERVER["REQUEST_URI"]);"
?>

<?php
$html = <<<HTML
<form name="myForm" method="post" >
<fieldset>
<legend>Please go back and correct following errors</legend>
<span class="error"> $surnameErr</span>
<span class="error"> $pnameErr </span>
<br>
<span class="error"> $titleErr</span>
<br>
<span class="error"> $genderErr </span>
<br>
<span class="error"> $yobErr </span>
<br>
<span class="error">$addressErr</span>
<br>
<span class="error">$suburbErr</span>
<br>
<span class="error"> $postcodeErr</span>
<br>
<span class="error"> $contactErr</span>
<br>
<span class="error"> $branchErr</span>
<br>
<span class="error"> $emailErr </span>
<br>
<span class="error">$confirmEmailErr</span>
<br>
<span class="error">$conditionErr</span>
<br>
<span class="error">$familyInfoErr</span>
</fieldset>
</form>
HTML;
?>


<?php if($submit != "correct"){ 
 echo $html ;
$goBack = <<<GOBACK
<form action="http://www.ozbizonline.com.au/?page_id=300" method="post">   
    <input type="Submit" value="Go Back">
</form>
GOBACK;
echo $goBack;
} else{ 
echo "<meta http-equiv='refresh' content='0;url=http://www.ozbizonline.com.au/440-2/'>";
exit();

  }
?>

<?php	while ( have_posts() ) {
		weaverx_post_count_clear(); the_post();

		get_template_part( 'templates/content', 'page' );

		comments_template( '', true );
	}

	weaverx_sb_postcontent('page');

	weaverx_page_tail( 'page', $sb_layout );    // end of page wrap
?>