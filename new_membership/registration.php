<?php
// Start the session
session_start();
?>
<?php
/*
Template Name: registration-form2
*/
?>
<?php
if ( !defined('ABSPATH')) exit;// Exit if accessed directly
/**
 * Standard page output (Default template)
 */
/*! ** DO NOT EDIT THIS FILE! It will be overwritten when the theme is updated! ** */
?> 
<?php	$sb_layout = weaverx_page_lead( 'page' ); ?>
<?php	weaverx_sb_precontent('page');?>
 <?php  $country = $_SESSION['country']; ?>
		  <table id="t02">
			   <tr>
					<td><img src="http://www.ozbizonline.com.au/wp-content/uploads/2016/10/Screen-Shot-2016-10-27-at-9.55.25-PM.png" alt="FORM Icon" width="40" height="40"><p id="text1">Select Membership <br>Type</p></td>
					<td><img src="http://www.ozbizonline.com.au/wp-content/uploads/2016/10/Screen-Shot-2016-10-27-at-9.55.54-PM.png" alt="FORM Icon" width="40" height="40"><p id="text2">Name And Address <br> Details</p></td>
					<td><img src="http://www.ozbizonline.com.au/wp-content/uploads/2016/10/Screen-Shot-2016-10-27-at-9.20.07-PM.png" alt="FORM Icon" width="40" height="40"><p id="text1">Summary of Your <br>Details</p></td>
					<td><img src="http://www.ozbizonline.com.au/wp-content/uploads/2016/10/Screen-Shot-2016-10-27-at-9.20.25-PM.png" alt="FORM Icon" width="40" height="40"><p id="text1">Payment <br>Details</p></td>
					<td><img src="http://www.ozbizonline.com.au/wp-content/uploads/2016/10/Screen-Shot-2016-10-27-at-9.20.34-PM.png" alt="FORM Icon" width="40" height="40"><p id="text1">Confirm <br>Application</p></td>
				</tr>
		 </table>
		
		<form id="registration-form" name="myForm"  onsubmit="return validateForm()" action="http://www.ozbizonline.com.au/?page_id=350" method="post">
			 <fieldset>
				<legend>Personal information:</legend>
				<div class="name">
			      <p id="pname"><label for="pname"> Prefered name for membership card:<input type="text" name="pname" placeholder="Prefered Name"></p></label>
				 <label for="surname" > <span>*</span>Surname:<input type="text" name="surname" id="surname" placeholder="Surname"></label>
				 </div>
				<p id="surnameErr"></p>
				<p> <label class = "field"><span>*</span>Given Names:<input type="text" name="forename" placeholder="Given Names"></p></label>
				<p id="forename"></p>
				<div class="titleAndgender">
						  <p id="gender-right">
							 <label class = "field"><span>*</span>Gender:
							 <select name="gender">
								 <option></option>
								 <option value="male">Male</option>
								 <option value="female">Female</option>
								 <option value="other">Other</option>
							 </select></label>
							
						 </p>
						 <label class = "field"><span>*</span>Title:
						 <select name="title" id="title">
							 <option></option>
							 <option value="mr">Mr</option>
							 <option value="mrs">Mrs</option>
							 <option value="ms">Ms</option>
							 <option value="dr">Dr</option>
							 <option value="oth" >Other</option>
						 </select> </label>
						
				</div>
				 <p id="titleError"></p>
				  <p id="genderError"></p>
			    <div id="otherType" style="display:none;">
					<p></p>
			        <label for="title_other">Enter Your Title:
					<input type="text" name="title_other" id="title_other" placeholder="title"/>  </label>
					<p></p>
			   </div>
			   <script>
                 var popUpList3 = jQuery('<div> <p> <label class = "field"> <span>*</span>Title:</label><input type="text" name="titleOther" id="titleOther"></p></div>');
					
					jQuery('#title').change(function () {
						 if (jQuery('#title').val() == 'oth') {
							    jQuery("#otherType").show()
						
						 }else{
							    jQuery("#otherType").hide() 
						 }
					});
               </script>
				<p>
				<label class = "field"><span>*</span>Year of Birth 
				<select id="birthyear" name="birthyear">
					<option></option>
					<option value="1998">1998</option>
					<option value="1997">1997</option>
					<option value="1996">1996</option>
					<option value="1995">1995</option>
					<option value="1994">1994</option>
					<option value="1993">1993</option>
					<option value="1992">1992</option>
					<option value="1991">1991</option>
					<option value="1990">1990</option>
					<option value="1989">1989</option>
					<option value="1988">1988</option>
					<option value="1987">1987</option>
					<option value="1986">1986</option>
					<option value="1985">1985</option>
					<option value="1984">1984</option>
					<option value="1983">1983</option>
					<option value="1982">1982</option>
					<option value="1981">1981</option>
					<option value="1980">1980</option>
					<option value="1979">1979</option>
					<option value="1978">1978</option>
					<option value="1977">1977</option>
					<option value="1976">1976</option>
					<option value="1975">1975</option>
					<option value="1974">1974</option>
					<option value="1973">1973</option>
					<option value="1972">1972</option>
					<option value="1971">1971</option>
					<option value="1970">1970</option>
					<option value="1969">1969</option>
					<option value="1968">1968</option>
					<option value="1967">1967</option>
					<option value="1966">1966</option>
					<option value="1965">1965</option>
					<option value="1964">1964</option>
					<option value="1963">1963</option>
					<option value="1962">1962</option>
					<option value="1961">1961</option>
					<option value="1960">1960</option>
					<option value="1959">1959</option>
					<option value="1958">1958</option>
					<option value="1957">1957</option>
					<option value="1956">1956</option>
					<option value="1955">1955</option>
					<option value="1954">1954</option>
					<option value="1953">1953</option>
					<option value="1952">1952</option>
					<option value="1951">1951</option>
					<option value="1950">1950</option>
					<option value="1949">1949</option>
					<option value="1948">1948</option>
					<option value="1947">1947</option>
					<option value="1946">1946</option>
					<option value="1945">1945</option>
					<option value="1944">1944</option>
					<option value="1943">1943</option>
					<option value="1942">1942</option>
					<option value="1941">1941</option>
					<option value="1940">1940</option>
					<option value="1939">1939</option>
					<option value="1938">1938</option>
					<option value="1937">1937</option>
					<option value="1936">1936</option>
					<option value="1935">1935</option>
					<option value="1934">1934</option>
					<option value="1933">1933</option>
					<option value="1932">1932</option>
					<option value="1931">1931</option>
					<option value="1930">1930</option>
					<option value="1929">1929</option>
					<option value="1928">1928</option>
					<option value="1927">1927</option>
					<option value="1926">1926</option>
					<option value="1925">1925</option>
					<option value="1924">1924</option>
					<option value="1923">1923</option>
					<option value="1922">1922</option>
					<option value="1921">1921</option>
					<option value="1920">1920</option>
					<option value="1919">1919</option>
					<option value="1918">1918</option>
					<option value="1917">1917</option>
					<option value="1916">1916</option>
					<option value="1915">1915</option>
					<option value="1914">1914</option>
					<option value="1913">1913</option>
					<option value="1912">1912</option>
					<option value="1911">1911</option>
					<option value="1910">1910</option>
					<option value="1909">1909</option>
					<option value="1908">1908</option>
					<option value="1907">1907</option>
					<option value="1906">1906</option>
					<option value="1905">1905</option>
					<option value="1904">1904</option>
					<option value="1903">1903</option>
					<option value="1902">1902</option>
					<option value="1901">1901</option>
					<option value="1900">1900</option>
				   </select> ( This is required for insurance purposes.)</label>
				  
				  <p id="yob"></p>
				  
				</p>
			 </fieldset>
			 <fieldset>
				<legend>Contact Details</legend>
				<p> <label class = "field"> Adress1:<input type="text" id="address1" name="address1"></p></label>
				<p> <label class = "field"> Adress2:<input type="text" id="address2" name="address2"></p></label>
				<p id = "addressErr"></p>
				<p> <label class = "field"> City/Suburb:<input type="text" id="suburb" name="suburb"></p></label>
				<p id="cityErr"></p>
				<p>
					<label class = "field"> State:
					<select id="state" name="state" onchange="validateState()">
						<option value="nsw">New South Wales</option>
						<option value="nt">Northern Territory</option>
						<option value="qld" selected>Queensland </option>
						<option value="sa">South Australia</option>
						<option value="tas">Tasmania</option>
						<option value="vic">Victoria</option>
						<option value="wa">Western Australia</option>
						<option value="other">other</option>
					</select></label>
				</p>
				<p id="state1"> </p>
				<p> <label class = "field"> Country:<input type="text" name="country" value = "<?php echo $country ; ?>" readonly > </p></label>
				<p> <label class = "field"> Post Code:<input type="text" id="post_code" name="post_code" oninput="validatePost()"></p></label>
				<p id="post1"></p>
				<label class = "field"> Postal address (if different then residential address):</label>
				<p>
				<textarea rows="4" cols="50" name="postal_address" id="postal_address">  </textarea>
				</p>
				<p> <label class = "field"><span>*</span> Email:<input type="text" name="emailID" placeholder="me@example.com"></p></label>
				<p id="emailID"></p>
				<p> <label class = "field"><span>*</span> Confirm Email:<input type="text" name="confirm_email"></p></label>
				<p id="confirm_email"></p>
				
			</fieldset>
			<fieldset>
				<legend>Contact Number</legend>
				<p id="phone_number_inline"> Home tel:<input type="number" id="code" name="telCode"  oninput="areaCodeValidate()" style="width: 2.5em;"> - <input type="number" id="tel" name="tel"  oninput="phoneValidate()" style="width: 5.5em;" ></p><br>
			    <p> <label class = "field"> Work tel:<input type="number" id="work_tel" name="work_tel"></p></label>
				<p> <label class = "field"> Mobile:<input type="number" id="mobile" name="mob"></p></label>
				  <p id="areaCode"></p>
				</fieldset>
			
			<fieldset>
		    <fieldset>
				<legend>Emergency Contact Details</legend>
				<p> <label class = "field"> Name:<input type="text" id="emergency_name" name="emergency_name" > </p></label>
			    <p> <label class = "field"> Phone number:<input type="number" id="emergency_number" name="emergency_number"></p></label>
				<p> <label class = "field"> Relationship:<input type="text" id="emergency_relationship" name="emergency_relationship"></p></label>
				  <p id="emergency_error"></p>
				</fieldset>
			
			<fieldset>
				<legend>Membership Detail</legend>
				<p> <label class = "field"> Select a membership option from each type:</label><br>
				<p>
				<fieldset>
					<p> <label class = "field"> <b>Membership type 1:</b></label><br>
					Direct <span id="directInfo" onclick="directMembership()">i</span><input type="radio" name="membership" id="direct" value="direct" checked>  <br>
					Branch<span id="branchInfo" onclick="branchMembership()">i</span><input type="radio" name="membership" id="branch"  value="branch" onchange="javascript:showhide('uniquename')">  <br>
					<br>
				    <p id="branchError"></p>
				</fieldset>
				<fieldset>
					<p> <label class = "field"> <b>Membership type 2:</b></label><br>
					 Single <span id="singleInfo" onclick="singleMembership()">i</span><input type="radio" name="membership2" id="single" value="single" checked><br>
					Family <span id="familyInfo" onclick="familyMembership()">i</span><input type="radio" name="membership2" id="familyMember" value="family" > <br>
					Life Enduring membership<input type="radio" name="membership2" id="lifeMember" value="life" >   <br>
				    </p>
				    <p id="familyError"></p>
				</fieldset>
				<fieldset>
					<p> <label class = "field"> <b>Membership Class:</b></label><br>
					Ordinary<input type="radio" name="membership_class" id="ordinary" value="ordinary" checked>  <br>
					Corporate<input type="radio" name="membership_class" id="corporate" value="corporate" >  <br>
				<!--	<input type="radio" name="membership_class" id="honorary" value="honorary life member" > Enduring Life membership <br> -->
					 Affiliate<input type="radio" name="membership_class" id="affiliate" value="affiliate" > <br>
				</p>
			     <div id="life_time" >
					<p></p>
					<fieldset>
					<p> <label class = "field"> <b>Membership Duration: </b></label>
					One Year <input type="radio" name="membership_duration" id="one" value="1" checked> <br>
					Three Year<input type="radio" name="membership_duration" id="three" value="3" > <br>
				   </p>
				   </fieldset>
					<p></p>
			   </div>
			   <script>
                jQuery('#lifeMember').click(function () {
						  jQuery("#life_time").hide()
					});
               </script>
			    <script>
                jQuery('#single').click(function () {
						  jQuery("#life_time").show()
					});
               </script>
				 <script>
                jQuery('#familyMember').click(function () {
						  jQuery("#life_time").show()
					});
               </script>
				<script>
					var popUpList = jQuery('<div><input type="radio" id="branchName" name="branchName" value="Beaudesert" >Beaudesert<br><input type="radio" value="NorthBrisbane" name="branchName">North Brisbane<br><input type="radio" value="Redlands" name="branchName">Redlands</div>');
					jQuery('#branch').change(function () {
						popUpList.dialog({
							modal: true,
							title: 'Select Branch',
							width: 400,
							buttons : {
								Ok: function() {
									var b = jQuery("input[name=branchName]:checked").val();
								    jQuery("#branchName2").val(b);
								    jQuery(this).dialog("close"); //closing on Ok click
								}
							},
	
						});
					});
               </script>
			   <script>
                 var popUpList2 = jQuery('<div> <p> <label class = "field"> <span>*</span>Surname:</label><input type="text" name="surnameF" id="surnameF"></p><br> <p> <label class = "field">Given names:</label><input type="text" name="forenameF" id="forenameF"></p><br><p> <label class = "field"> Prefered name:</label><input type="text" name="pnamef" id="pnamef"></p><p><label class = "field"> <span>*</span>Title:</label><select name="titleF" id="titleF"><option value="mr">Mr</option><option value="mrs">Mrs</option><option value="miss">Ms</option><option value="miss">Dr</option></select></p><p><label class = "field"> <span>*</span>Gender:</label><select name="genderF" id="genderF"><option value="male">Male</option><option value="female">Female</option><option value="other">Other</option></select></p><p> <label class = "field"> <span>*</span>Year of birth:</label><input type="text" name="yobF" id="yobF" placeholder="eg. 1988"></p><p> <label class = "field"> <span>*</span>Mobile:</label><input type="text" name="mobf" id="mobf"></p></div>');
					jQuery('#familyMember').change(function () {
						popUpList2.dialog({
							modal: true,
							title: 'Enter Family Member details',
							width: 400,
							buttons : {
								Ok: function() {
								    var sname = jQuery('#surnameF').val();
									var fname = jQuery('#forenameF').val();
									var pname = jQuery('#pnamef').val();
									var tname = jQuery('#titleF').val();
									var gend = jQuery('#genderF').val();
									var yearf = jQuery('#yobF').val(); 
									var mobf = jQuery('#mobf').val();
								
								    jQuery("#surnameFamily").val(sname);
									jQuery("#foreNameFamily").val(fname);
									jQuery("#preferNameFamily").val(pname);
									jQuery("#titleFamily").val(tname);
									jQuery("#genderFamily").val(gend);
									jQuery("#yobFamily").val(yearf);
								    jQuery("#mobFamily").val(mobf);
									jQuery(this).dialog("close"); //closing on Ok click
								}
							},
	
						});
					});
               </script>
			  
			  
			   <!-- hidden-text box to get the value of branch name  -->
			   <input type="hidden" name="branchName2" id="branchName2" ><br>
			   <input type="hidden" name="stateOther" id="stateOther">
			    <!-- hidden-text box to get the value of family Membr's details -->
				<input type="hidden" name="surnameFamily" id="surnameFamily">
				<input type="hidden" name="foreNameFamily" id="foreNameFamily">
				<input type="hidden" name="preferNameFamily" id="preferNameFamily">
				<input type="hidden" name="titleFamily" id="titleFamily">
				<input type="hidden" name="genderFamily" id="genderFamily">
				<input type="hidden" name="yobFamily" id="yobFamily">
				<input type="hidden" name="mobFamily" id="mobFamily">
				<!--
					<input type="radio" name="membership" id="membership" value="branch"  onchange="javascript:showhide('uniquename')"> Branch <span id="branch" >i</span><br>
	            
				    
					<div id="uniquename" style="display:none;">
						 <p id="branch-name">
						 <label class = "field">Select a Branch Name if you have selected branch membership above:</label><br>
						<input type="radio" name="branchName" value="beaudesert" >Beaudesert<br>
						<input type="radio" value="North Brisbane" name="branchName">North Brisbane<br>
						<input type="radio" value="Redlands" name="branchName">Redlands
						</p>
					</p>
				-->
			</fieldset>
			<fieldset>
				<legend>Advertisement</legend>
				<p>
					<label class = "field">How did you find about us:
					<select name="find_us" id="find_us">
						<option value="advertising">Advertising</option>
						<option value="friend">Friend</option>
						<option value="library display">Library Display</option>
						<option value="website">Web Site</option>
						<option value="search_engine">Search Engine</option>
						<option value="newspaper">Newspaper article</option>
						<option value="event">GSQ event</option>
						<option value="oth_findus">Other</option>
					</select></label>
				</p>
				 <div id="other_findus" style="display:none;">
					<p></p>
			        <label for="title_other">How did you find about us: 
					<input type="text" name="findus_other" id="findus_other" /> </label>
					<p></p>
			   </div>
			   <script>
                 jQuery('#find_us').change(function () {
						 if (jQuery('#find_us').val() == 'oth_findus') {
							    jQuery("#other_findus").show()
			   
						 }else{
							    jQuery("#other_findus").hide() 
						 }
					});
               </script>
			   <br>
			 
				 <label class = "field"> Interest areas eg Ireland 1845-1890 O'Reilly, Hanlon
				 <p><textarea rows="4" cols="30" name="interest" id="interest"></textarea> </p></label>
			 
			</fieldset>
			<fieldset>
			<legend>Terms and Conditions</legend>
			<p>
				You must accept our<a href="https://www.gsq.org.au/index.php/terms-and-conditions.html" target="_blank"> <span id="terms">Terms and Conditions</span> </a> a pre-requisite to registration. Please make sure you have read them thoroughly.
				By clicking in the following box,you acknowledge reciept of the advice about public liability insurance and agree to the terms and conditions.
				
				I/We hereby apply for membership of the Genealogical Society of Queensland Inc and agree to be bound by the rules of the society.
				I/We acknowlegde advice from about the the public liability insurance and agree to the Terms and Conditions.
				<p>
				<br><a href="https://www.gsq.org.au/index.php/terms-and-conditions.html" target="_blank"> <span id="terms">Shop Terms and Conditions</span> </a>
			
					<a id="membershipTerms" href="https://www.gsq.org.au/index.php/terms-and-conditions.html" target="_blank"> <span id="terms">Membership Terms and Conditions</span> </a>
				</p>
				 Accept<input type="radio" name="condition" value="agree" id="conditionAccepted"> <br>
				 Don't Accept<input type="radio" name="condition" value="agreeNot">
				<p id="conditionError"></p>
				<p id="memberContact">Contact: membership@gsq.org.au</p>
			</p>
			</fieldset>
			<input type="submit" class="sendIt" value="Submit">
			
		</form>
    
<?php	while ( have_posts() ) {
		weaverx_post_count_clear(); the_post();

		get_template_part( 'templates/content', 'page' );

		comments_template( '', true );
	}

	weaverx_sb_postcontent('page');

	weaverx_page_tail( 'page', $sb_layout );    // end of page wrap
?>
