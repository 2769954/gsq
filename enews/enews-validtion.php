
<?php
// Start the session
session_start();
// This file validates input information for enews and save to the database
?>
<?php
/*
Template Name: enews details validate and save to database  
*/
?>

<?php
//error handler function
function customError($errno, $errstr) {
  echo "<b>Error:</b> [$errno] $errstr";
}

//set error handler
set_error_handler("customError");

?>
<?php
if ( !defined('ABSPATH')) exit; // Exit if accessed directly
/**
 * Standard page output (Default template)
 */
?>
<?php	$sb_layout = weaverx_page_lead( 'page' ); ?>
<?php	weaverx_sb_precontent('page');?>

<?php
// define variables and set to empty values
 $firstname = $lastname = $email ="";
 $surnameErr = $pnameErr = $emailErr = "";
$submit = "correct" ;

if ($_SERVER["REQUEST_METHOD"] == "POST") {
   
        if (empty($_POST["enews_fname"])) {
           $surnameErr = "* first name is required";
           $submit = "surname" ;
           
         } else {
           $firstname = test_input($_POST["enews_fname"]);
           // check if name only contains letters and whitespace
           if (!preg_match("/^[a-zA-Z ]*$/",$firstname)) {
             $surnameErr = "* Only letters and white space allowed in name";
             $submit = "surname" ;
           }
         }
         
          if (empty($_POST["enews_lname"])) {
           $pnameErr= "* Last name is required";
           $submit = "last name" ;
           
         } else {
           $lastname = test_input($_POST["enews_lname"]);
           // check if name only contains letters and whitespace
           if (!preg_match("/^[a-zA-Z ]*$/",$lastname)) {
             $pnameErr = "* Only letters and white space allowed in name";
             $submit = "last name" ;
           }
         }
         
         if (empty($_POST["enews_email"])) {
           $emailErr = "* Email is required";
           $submit = "email";
         } else {
           $email = test_input($_POST["enews_email"]);
           
           // check if e-mail address is well-formed
           if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
             $emailErr = "* Invalid email format";
             $submit = "email";
           }
         }
 
}

// validate all input data for illegell characters
function test_input($data) {
   $data = trim($data);
   $data = substr($data, 0, 500);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  $data = EscapeShellCmd($data);
  return $data;
}

?>

<?php
$html = <<<HTML
<form name="myForm" method="post" >
<fieldset>
<legend>Please go back and correct following errors</legend>
<span class="error"> $surnameErr</span>
<span class="error"> $pnameErr </span>
<br>
<span class="error"> $emailErr </span>
<br>
</fieldset>
</form>
HTML;
?>


<?php if($submit != "correct"){ 
//echo $submit; 
echo $html ;
$goBack = <<<GOBACK
<form action="http://www.ozbizonline.com.au" method="post">   
    <input type="Submit" value="Go Back">
</form>
GOBACK;
echo $goBack;
} else{
  
	 // save user to gsq_users table in database
	 $servername = "localhost" ;
    $username = "gsqtest_gsqAdmin";
    $password = "gsqProject2016";
    $dbname = "gsqtest_users";

// Create connection
			$conn = new mysqli($servername, $username, $password, $dbname);
				// Check connection
			if ($conn->connect_error) {
				die("Connection failed: " . $conn->connect_error);
			} 
			$firstname = mysqli_real_escape_string($conn, $firstname);
			$lastname = mysqli_real_escape_string($conn, $lastname);
			$email = mysqli_real_escape_string($conn, $email);
            
            $sql = "INSERT INTO gsq_enews
					( first_name,last_name,email)
					 VALUES ( ?,?,?)";
			 // Attempt to prepare the query
			 if ($stmt = $conn->prepare($sql))
			 {
						$stmt->bind_param("sss", $firstname , $lastname,$email);
						$stmt->execute();
						$last_id = $conn->insert_id;
						if($last_id == 0){
							
							// error occured while saving in database probabl reason unique email id constraint
							 die("An error occured");
						}
						 
						 $stmt->close();?>
						<h3>You have successfully subscribed for enews.</h3>
						 
						 
<?php	 	} else {	
			     echo "Error: " . $sql . "<br>" . $conn->error;
			}


  }
?>

<?php	while ( have_posts() ) {
		weaverx_post_count_clear(); the_post();

		get_template_part( 'templates/content', 'page' );

		comments_template( '', true );
	}

	weaverx_sb_postcontent('page');

	weaverx_page_tail( 'page', $sb_layout );    // end of page wrap
?>