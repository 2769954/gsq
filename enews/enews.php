<?php
/*
//phpinfo();
error_reporting(E_ALL);
ini_set( 'display_errors','1');
*/
?>
<?php
// Start the session
session_start();
?>
<?php
/*
Template Name: e news input form at home page
*/
?>

<?php
/*
//error handler function
function customError($errno, $errstr) {
  echo "<b>Error:</b> [$errno] $errstr";
}

//set error handler
set_error_handler("customError");
*/
?>
<?php
if ( !defined('ABSPATH')) exit; // Exit if accessed directly
/**
 * Standard page output (Default template)
 */
?>
<?php	$sb_layout = weaverx_page_lead( 'page' ); ?>
<?php	weaverx_sb_precontent('page');?>
 <?php	while ( have_posts() ) {
		weaverx_post_count_clear(); the_post();

		get_template_part( 'templates/content', 'page' );

		comments_template( '', true );
	}

	weaverx_sb_postcontent('page');
	?>


	<div class="enews" >
        <form id="enews_form" name="enews_form" onsubmit="return validateEnews()" action="http://www.ozbizonline.com.au/e-news/"  method="post"  style='display:inline;'>
            <p id=enews_form_color>
              <input type="text" name="enews_fname" id="enews_fname"  placeholder="First Name" >
              <input type="text" name="enews_lname" id="enews_lname" placeholder="Last Name">
              <input type="text" name="enews_email" id="enews_email" placeholder="Email">
              <input type="submit" name="enews_btn" id="enews_btn" value="Sign Up for E-News">
            </p>
       </form>
		   <p id="enews_firstname_error"></p>
           <p id="enews_lastname_error"></p>
          <p id="enews_email_error"></p>
		<p></p>
    </div>


<p></p>

<?php
	weaverx_page_tail( 'page', $sb_layout );    // end of page wrap
?>